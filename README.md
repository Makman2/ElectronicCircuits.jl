# ElectronicCircuits.jl

ElectronicCircuits.jl is a non-linear electronic circuit simulation framework.

Currently, it features following analysis types:
- transient analysis (`simulate!`)
- transient stead-state analysis (`steadystate!`)
- transient analysis via EFM (Envelope Following Method) for multi-rate/multi-frequency systems (`simulate_efm!`)
- EFM steady-state analysis (`multirate_steadystate!`)

ElectronicCircuits.jl provides you with a solver interface where you can easily
write your own component types from a known model.

## Usage

The workflow to create a simulation is usually as follows:

1. Instantiate desired components (such as `Resistor`, `Inductor` or `Capacitor`).
2. Connect them to create a circuit topology (via `connect!()`).
3. Compile a model with `compile()`.
4. Invoke simulation methods on the returned model.
5. Optional: Plot

```julia
using ElectronicCircuits
using Plots

# Create circuit!
f = 10000.0
src = SinusoidalVoltageSource(f, 10.0)
r1 = Resistor(100.0)
c1 = Capacitor(1e-7, 10.0)

circuit = Circuit()
connect!(circuit, src.pins.pos, r1.pins.a)
connect!(circuit, r1.pins.b, c1.pins.a)
connect!(circuit, c1.pins.b, src.pins.neg)

# Simulate!
model = compile(circuit)
@time sim = simulate!(model, 0.0, 5.0 / f, 100)

# Plot!
voltages = plot(get_times(sim), get_voltages(sim, src), label="E: v(t)");
plot!(get_times(sim), get_voltages(sim, r1), label="R₁: v(t)");
plot!(get_times(sim), get_voltages(sim, c1), label="C₁: v(t)");
currents = plot(get_times(sim), get_currents(sim, src), label="E₁: i(t)");
plot!(get_times(sim), get_currents(sim, r1), label="R₁: i(t)");
plot!(get_times(sim), get_currents(sim, c1), label="C₁: i(t)");
plot(voltages, currents, layout=(2, 1))
```

More examples can be found on the [snippets page](https://gitlab.com/Makman2/ElectronicCircuits.jl/-/snippets).

## Documentation

Documentation can is generated via `Documenter`.

You can find an online version [here](https://Makman2.gitlab.io/ElectronicCircuits.jl).

Additionally, as this package is a port of the Python reference implementation [respice](https://gitlab.com/Makman2/respice),
you can also refer to the [documentation there](https://makman2.gitlab.io/respice).
