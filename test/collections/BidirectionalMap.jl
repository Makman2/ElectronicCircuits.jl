@testset "BidirectionalMap" begin
    map = BidirectionalMap()

    @test isempty(map)
    empty!(map)
    @test isempty(map)

    map[0] = 1
    @test map[0] == 1
    @test map.obverse[0] == 1
    @test map.reverse[1] == 0
    map[2] = 3
    @test map[2] == 3
    @test map.obverse[2] == 3
    @test map.reverse[3] == 2
    map[0] = 2
    @test map[0] == 2
    @test map.obverse[0] == 2
    @test map.reverse[2] == 0
    @test !haskey(map.reverse, 1)
end
