import CSV


@testset "Reference Models: Buck-converter with motor load (EFM Multi-rate steady-state analysis)" begin
    E = 100.0
    fₖ = 100e3  # Converter frequency.
    fₜ = 10.0  # Applied torque frequency.
    Tₖ = 1.0 / fₖ
    Tₜ = 1.0 / fₜ

    src = RectangularVoltageSource(fₖ, 0.5E, 0.0, 0.5E, 0.3)
    l₁ = Inductor(420e-6)
    c₁ = Capacitor(38e-6)
    load_torque(t::Float64) = 5.0 + sin(2pi * fₜ * t)
    motor = Motor(0.425, 0.00378, 0.466, 0.466, 0.02255, 0.001, load_torque)

    circuit = Circuit()
    connect!(circuit, src.pins.pos, l₁.pins.a)
    connect!(circuit, l₁.pins.b, c₁.pins.b, motor.pins.pos)
    connect!(circuit, src.pins.neg, c₁.pins.a, motor.pins.neg)

    # Include a little time offset to properly resolve the rectangular source peaks.
    # Otherwise results get a bit shaky.
    t₀ = 0.001 * Tₖ

    model = compile(circuit)
    simulate_efm!(model, t₀, Tₖ, 1000, 20, 2)  # Presimulate for faster results.
    sim = multirate_steadystate!(model, t₀, [Tₖ, Tₜ], 2, 10)

    testdata = CSV.File(joinpath(@__DIR__, "testdata", "buckmotor_efm_steadystate.csv"), type=Float64, strict=true)

    @test get_times(sim) ≈ testdata.t
    @test get_voltages(sim, src) ≈ testdata.vE
    @test get_currents(sim, src) ≈ testdata.iE
    @test get_voltages(sim, c₁) ≈ testdata.vc₁
    @test get_currents(sim, c₁) ≈ testdata.ic₁
    @test get_voltages(sim, l₁) ≈ testdata.vl₁
    @test get_currents(sim, l₁) ≈ testdata.il₁
    @test get_voltages(sim, motor) ≈ testdata.vmotor
    @test get_currents(sim, motor) ≈ testdata.imotor
    @test get_states(sim, motor, 3) ≈ testdata.ω

    # Consistency tests.
    @test get_currents(sim, src) ≈ get_currents(sim, l₁)
    @test get_currents(sim, l₁) ≈ -(get_currents(sim, motor) + get_currents(sim, c₁))
    @test get_voltages(sim, c₁) ≈ get_voltages(sim, motor)
end
