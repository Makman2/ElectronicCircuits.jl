import CSV


@testset "Reference Models: EFM analysis of multi-rate RLC circuit" begin
    f_slow = 50.0
    f_fast = 10000.0

    v_src_slow = RectangularVoltageSource(f_slow, 10.0, 0.1)
    v_src_fast = RectangularVoltageSource(f_fast, 1.0, 0.1)
    r1 = Resistor(30.0)
    c1 = Capacitor(1e-6)
    l1 = Inductor(80e-3)

    circuit = Circuit()
    connect!(circuit, v_src_slow.pins.pos, v_src_fast.pins.neg)
    connect!(circuit, v_src_fast.pins.pos, r1.pins.a)
    connect!(circuit, r1.pins.b, c1.pins.a)
    connect!(circuit, c1.pins.b, l1.pins.a)
    connect!(circuit, l1.pins.b, v_src_slow.pins.neg)

    model = compile(circuit)
    sim = simulate_efm!(model, 0.0, 1.0 / f_fast, 4, 30, 4)

    testdata = CSV.File(joinpath(@__DIR__, "testdata", "efm_switched_rlc.csv"), type=Float64, strict=true)

    @test get_times(sim) ≈ testdata.t
    @test get_voltages(sim, v_src_slow) ≈ testdata.vE₁
    @test get_currents(sim, v_src_slow) ≈ testdata.iE₁
    @test get_voltages(sim, v_src_fast) ≈ testdata.vE₂
    @test get_currents(sim, v_src_fast) ≈ testdata.iE₂
    @test get_voltages(sim, r1) ≈ testdata.vr₁
    @test get_currents(sim, r1) ≈ testdata.ir₁
    @test get_voltages(sim, c1) ≈ testdata.vc₁
    @test get_currents(sim, c1) ≈ testdata.ic₁
    @test get_voltages(sim, l1) ≈ testdata.vl₁
    @test get_currents(sim, l1) ≈ testdata.il₁

    # Consistency tests.
    @test get_currents(sim, v_src_slow) ≈ get_currents(sim, v_src_fast) ≈ get_currents(sim, r1) ≈ get_currents(sim, c1) ≈ get_currents(sim, l1)
end
