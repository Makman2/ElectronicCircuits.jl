@testset "Reference Models: Resistors in series" begin
    r1 = Resistor(100.0)
    r2 = Resistor(200.0)
    r3 = Resistor(700.0)
    src = ConstantVoltageSource(20.0)

    circuit = Circuit()
    connect!(circuit, src.pins.pos, r1.pins.a)
    connect!(circuit, r1.pins.b, r2.pins.a)
    connect!(circuit, r2.pins.b, r3.pins.a)
    connect!(circuit, r3.pins.b, src.pins.neg)

    model = compile(circuit)
    sim = simulate!(model, 0.0, 1.0, 10)

    @test get_voltages(sim, src) ≈ ones(10) * 20.0
    @test get_currents(sim, src) ≈ -ones(10) * 0.02
    @test get_voltages(sim, r1) ≈ ones(10) * -2.0
    @test get_currents(sim, r1) ≈ get_currents(sim, src)
    @test get_voltages(sim, r2) ≈ ones(10) * -4.0
    @test get_currents(sim, r2) ≈ get_currents(sim, src)
    @test get_voltages(sim, r3) ≈ ones(10) * -14.0
    @test get_currents(sim, r3) ≈ get_currents(sim, src)
end
