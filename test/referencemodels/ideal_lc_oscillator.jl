@testset "Reference Models: Ideal LC oscillator" begin
    c₁ = Capacitor(1e-7, 10.0)
    l₁ = Inductor(1e-3)

    circuit = Circuit()
    connect!(circuit, c₁.pins.a, l₁.pins.a)
    connect!(circuit, c₁.pins.b, l₁.pins.b)

    model = compile(circuit)
    sim = simulate!(model, 0.0, 0.0003, 1000)

    # Ideal solution:
    ω = 1 / √(c₁.value * l₁.value)
    vc = (t) -> 10.0 * cos(ω * t)
    ic = (t) -> -ω * 10.0 * c₁.value * sin(ω * t)

    # Solution precision from ideal one decreases over time, so the approximation tolerances become relatively high.
    @test isapprox(get_voltages(sim, c₁), vc.(get_times(sim)); rtol = 1e-2)
    @test isapprox(get_currents(sim, c₁), ic.(get_times(sim)); rtol = 1e-2)
    @test get_voltages(sim, l₁) ≈ get_voltages(sim, c₁)
    @test get_currents(sim, l₁) ≈ -get_currents(sim, c₁)
end
