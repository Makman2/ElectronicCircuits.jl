@testset "Reference Models: RLC circuit steady-state analysis" begin
    src = SinusoidalVoltageSource(10000.0, 10.0)
    r1 = Resistor(100.0)
    c1 = Capacitor(1e-7, 10.0)

    circuit = Circuit()
    connect!(circuit, src.pins.pos, r1.pins.a)
    connect!(circuit, r1.pins.b, c1.pins.a)
    connect!(circuit, c1.pins.b, src.pins.neg)

    model = compile(circuit)
    sim = steadystate!(model, 0.0, 0.0001, 100)

    @test get_steadystate(sim, c1) ≈ [4.505428550975888, -0.04505428550965702]
    @test get_voltages(sim, c1)[begin] ≈ get_voltages(sim, c1)[end]
    @test get_currents(sim, c1)[begin] ≈ get_currents(sim, c1)[end]
end
