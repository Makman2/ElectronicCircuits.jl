using ElectronicCircuits
using Test

include("collections/BidirectionalMap.jl")

include("referencemodels/buckmotor_efm_steadystate.jl")
include("referencemodels/efm_switched_rlc.jl")
include("referencemodels/ideal_lc_oscillator.jl")
include("referencemodels/rlc_steadystate.jl")
include("referencemodels/r_series.jl")
