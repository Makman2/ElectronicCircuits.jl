# Contributing

Contributions to this project are highly welcome!
If you have new ideas, features, ..., don't hesitate and contribute! :D

If you don't know where to start, I would first recommend:
- Roughly scan through the documentation to get an overview of how this package is constructed.
- Execute a few sample scripts from the [snippets page](https://gitlab.com/Makman2/ElectronicCircuits.jl/-/snippets).
- Run tests (see [Testing](@ref)).
- If you want to work on existing problems, you can go through the [issues list](https://gitlab.com/Makman2/ElectronicCircuits.jl/-/issues).
- Don't hesitate to ask and contact me :)

  You can find contact details on [my GitLab profile page](https://gitlab.com/Makman2).

You can always just submit PRs, not only for pure logic code, but also for docs!
If something is unclear, it is probably not well enough written.
Feel free to [create an issue](https://gitlab.com/Makman2/ElectronicCircuits.jl/-/issues/new), or even better
and highly encouraged, extend the documentation yourself :)

Happy hacking!
