# Documentation Generation

Documentation can is generated via [`Documenter.jl`](https://juliadocs.github.io/Documenter.jl/stable/).

To separate unnecessary dependencies not needed by the standard package user,
another environment is available inside the `docs` directory.

To compile the documentation, execute:

```shell
julia --project=docs docs/make.jl
```

The output files are placed into the `docs/build` directory.

Documentation is automatically deployed to the [GitLab pages default domain](https://Makman2.gitlab.io/ElectronicCircuits.jl).

For more info, see the [`Documenter.jl` documentation](https://juliadocs.github.io/Documenter.jl/stable/).
