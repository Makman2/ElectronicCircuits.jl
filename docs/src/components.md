# Components

ElectronicCircuits defines components as a building block to interface with the simulation.

Available components are:

- `Resistor`, `Inductor`, `Capacitor`: The classic ideal passive elements.
- `Motor`: Asynchronous motor model.
- `Diode`: A diode based on the Shockley model.
- Voltage and current sources:
  - `ConstantVoltageSource`/`ConstantCurrentSource`: Standard DC voltage source.
  - `SinusoidalVoltageSource`: Standard AC voltage source.
  - `RectangularVoltageSource`: A PWM modulated source with adjustable duty-cycle.

## State

State denotes the dynamic state of a component. Components that change their behavior over
time such as capacitors are stateful components.
For example, the stored charge inside a capacitor can vary over time.

## Custom Components

In general, it is recommended to implement a component like below.
Note, that this is just a template, you can replace state/pin/property names
as desired.

```julia
# Only implement a state if your component is stateful.
# E.g. resistors are stateless, but capacitors and inductors are dynamic
# and thus have state.
mutable struct MyComponentState
    state1::Float64
    state2::Float64
    # ...
end

# It is recommended to group pins in a separate structure like this:
struct MyComponentPins
    negative::Pin
    positive::Pin
    neutral::Pin
    # ... or any other pins you might need or don't need.

    # For each pin, new() should be called with a `Pin(component)` to initialize
    # a new, unique pin the circuit can connect to. Never share pins between components.
    MyComponentPins(component::Component) = new(Pin(component), Pin(component), Pin(component))
end

export MyComponent
mutable struct MyComponent <: Component
    frequency::Float64
    amplitude::CapacitorState
    # ... you name it. You can add properties as required by your model.

    pins::MyComponentPins
    state::MyComponentState  # If your component does not have a state, omit this field.

    # Adjust the signature to your likings and if possible, add as many sane defaults as you can
    # to make it easier to use your custom component. You can also add other constructors.
    Capacitor(frequency::Float64, amplitude::Float64, state1::Float64 = 0.0, state2::Float64) = (component = new(value, MyComponentState(state1, state2)); component.pins = MyComponentPins(component); component)
end
```

To implement components, you have to define the interface imposed by `components.jl`.

- `branches(component::MyComponent, pinmap::AbstractDict{Pin, Node})::Array{Branch, 1}`

  Defines the type of "branches" the component connects into a circuit.

  A component can define multiple branches. Depending on your model equations, you
  have to choose the right branches, so your model correctly receives either desired voltages
  or currents. Following branch types are available:
  - `VoltageBranch(source::Node, target::Node)`: Receives the current flowing from `source` to `target` and requires to output voltage depending on current.
  - `CurrentBranch(source::Node, target::Node)`: Receives the voltage between `source` and `target` and requires to output current depending on voltage.

  The `branches()` function is expected to return one or multiple branches, where the order
  is important later for the feature vector `x`.

  As can be seen in the signature, you are required to create new branches and giving them nodes of type `Node`.
  Depending on what pins are connected, the model will create common nodes for you that you can use
  via the `pinmap`.

  As an example, consider a bipolar junction transistor model based on the [Ebers-Moll model](https://en.wikipedia.org/wiki/Bipolar_junction_transistor#Ebers%E2%80%93Moll_model) with source, base and emitter pins.

  ```math
  \begin{align*}
      i_{\text{C}} &= I_\text{S} \left[ \left(e^\frac{V_\text{BE}}{V_\text{T}} - e^\frac{V_\text{BC}}{V_\text{T}}\right) - \frac{1}{\beta_\text{R}} \left(e^\frac{V_\text{BC}}{V_\text{T}} - 1\right) \right]\\
      i_{\text{E}} &= I_\text{S} \left[ \left(e^\frac{V_\text{BE}}{V_\text{T}} - e^\frac{V_\text{BC}}{V_\text{T}}\right) + \frac{1}{\beta_\text{F}} \left(e^\frac{V_\text{BE}}{V_\text{T}} - 1\right) \right]
  \end{align*}
  ```

  To model a BJT accordingly, you could specify following branch layout:

  ```julia
  branches(component::MyTransistor, pinmap::AbstractDict{Pin, Node})::Array{Branch, 1} = [
      CurrentBranch(pinmap[component.pins.base], pinmap[component.pins.collector]),  # BC (base-collector) branch
      CurrentBranch(pinmap[component.pins.base], pinmap[component.pins.emitter])  # BE (base-emitter) branch
  ]
  ```

  In the other methods, the feature vector `x` would now receive a vector with 2 voltages `[v₁, v₂]`,
  where v₁ is the voltage as specified by the first branch between the base and collector,
  and v₂ is the voltage specified by the second branch between the base and emitter.

  You can mix branch types as desired, ElectronicCircuits will take care of passing their
  voltages and currents to `x` in order.

- `output(component::MyComponent, x::Vector{Float64}, t₁::Float64, t₂::Float64)::Vector{Float64}`

  The most important function. Defines the output feature vector,
  i.e. output voltages and currents depending on the input voltages and currents `x`.
  Depending on the order of branches given in `branches()`, `x` will receive the according
  voltages and currents.

  For the BJT example, the output function could look like

  ```julia
  function output(component::MyTransistor, x::Vector{Float64}, t₁::Float64, t₂::Float64)::Vector{Float64}
      exp_bc_term, exp_be_term = @. exp(x / component.Vₜ)
      return component.Iₛ * [
          exp_be_term - exp_bc_term - (exp_bc_term - 1.0) / component.βR,
          exp_be_term - exp_bc_term + (exp_be_term - 1.0) / component.βF,
      ]
  end
  ```

  Where `Iₛ`, `βR`, `βF`, `Vₜ` are BJT-characteristic model properties:
  reverse saturation current, reverse common emitter gain, forward common emitter gain and thermal voltage.

- State getter and setter

  If your model has state, to support steady-state analysis, you must provide

  - `get_state(component::Component)::Vector{Float64}`
  - `set_state!(component::Component, state::Vector{Float64})`

  By default and for the case when your custom component is stateless, there are
  default implementations that do nothing.

  Otherwise, referring to the top-most custom component template code, those functions
  only get and set the state in a reliable order and usually just look like this:

  ```julia
  get_state(component::MyComponent)::Vector{Float64} = [component.state.state1, component.state.state2]
  function set_state!(component::MyComponent, state::Vector{Float64})
      component.state.state1, component.state.state2 = state
  end
  ```

  For the BJT example case, there is no state to declare, so you don't have to
  add these methods.

- update!(component::Component, x::Vector{Float64}, t₁::Float64, t₂::Float64)

  If your component has state, it must be updated in the update-step of the solver to
  progress the simulation and properly simulate dynamic behavior.

  The default implementation does nothing and does not need specific methods for your
  custom component if you have no state.

- Discontinuities

  You need to add "events" to your component if there appear discontinuous jumps.

  ```julia
  next_event(component::MyComponent, t₁::Float64, t₂::Float64)::Union{Float64, Nothing}
  ```

  By default, the default implementation returns `nothing`, denoting no events at all.

  Usually, PWM sources or other non-smooth sources will implement this function (such as `RectangularVoltageSource`).

- show(io::IO, component::MyComponent) & show(io::IO, component::MyComponentState)

  It is recommended that you add methods to `show` so you can work with the components
  easier in the REPL. Include the component's properties and add proper units to them to reflect
  the physical model as good as possible.

  For example, the BJT `show` method could look like:

  ```julia
  function show(io::IO, component::MyTransistor)
      print(io, "$(typeof(component))(Iₛ=$(component.Iₛ)A, βF=$(component.βF), βR=$(component.βR), Vₜ=$(component.Vₜ)V)")
  end
  ```

For reference, you can look at `Resistor`, `Capacitor` or other component
implementations on how the interface is to be implemented.
