# Testing

To execute test, start Julia:

```shell
julia --project=.
```

From the REPL, enter the `Pkg` environment (type `]`) and do:

```julia-repl
pkg> test
```
