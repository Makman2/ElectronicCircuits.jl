# Solver Cycle

ElectronicCircuits uses a custom algorithm to solve circuits.
In general, non-linear circuits are supported by assembling an
in-place discretized DAE (differential algebraic equation)
that is solved via the `NLSolve` package.

The kind of discretization is up to the component implementation. In general, for continuous
components, it is recommended to use the
[trapezoidal type discretization](https://en.wikipedia.org/wiki/Trapezoidal_rule_(differential_equations)).

## Circuit Model Assembly

When invoking `compile` to create a model, following steps are performed:

1. From connected components' pins inside a `Circuit` instance, common nodes
   are generated in form of a mapping, the so called pinmap.
   The pinmap defines the relation of an individual component pin to the actual circuit node.

2. Components return their *branches*. These define whether to receive currents or voltages from
   which nodes in the output function and in what order.

3. Inside the model compile step, ground nodes (GND) are automatically chosen.

4. The DAE equations are generated by assembling currents and voltages together from the
   connected components according the [MNA (modified nodal analysis)](https://en.wikipedia.org/wiki/Modified_nodal_analysis)
   and runtime compiled via `eval`.

5. The model structure is populated with all necessary functions
   (DAE current and voltage residual function, alias the MNA master equation, and other ones that were beneficial for performance to compile)
   and attributes to launch simulations.

## Transient Simulation

1. Generate list of time points to simulate.
2. Gather initial events occurring in the initial range (this is a special edge-case for events that needs special handling).
3. Use zero as an initial guess for the residual.
4. Solve the residual (MNA master equation) at time ``t=0`` with `nlsolve` (calls `output(comopnent, ...)`).
5. Store the results.
6. Update components (`update!(component, ...)`)
7. Set new initial guess to previous result.
8. Rinse and repeat for each time step:
   1. Check for events. If there are events, insert time-steps accordingly and adjust simulation time-frame.
   2. Solve the residual (MNA master equation) with `nlsolve` (calls `output(comopnent, ...)`).
   3. Store the results.
   4. Update components (`update!(component, ...)`)
   5. Set new initial guess to previous result.
9. Assemble final result object and populate with solved simulation data.

## Discontinuous Events

Events are discontinuous behavior at certain time-points.
If no additional time-steps are inserted there, the simulation might produce very wrong
results, especially for power electronic simulation with many discontinuous sources as they
appear in switched-mode devices.

Handling events is done by checking if there are events within the simulation time-frame
by calling each component's `next_event()` method. If `nothing` is returned, no event is detected
inside the simulation time-frame. If there is an event, 2 additional time-steps are inserted
around the event, at ``t_e - ERP`` and ``t_e + ERP``, where ``ERP`` means the so called
"event resolution precision" which defines a small enough value where events are likely to resolve well.
ERP is necessary because small numerical limitation in algebraic functions cause, that
the actual and mathemtically correct events and the calculated ones do not align exactly.
