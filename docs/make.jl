using Documenter, ElectronicCircuits

projectname = "ElectronicCircuits.jl"

makedocs(
    sitename = "$projectname Documentation",
    authors = "Mischa Aleksej Krüger",
    repo = "https://gitlab.com/Makman2/$projectname/blob/{commit}{path}#{line}",
    modules = [ElectronicCircuits],
)
