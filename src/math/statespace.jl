using LinearAlgebra


"""
Returns the next numerical integration step for a linear differential equation system.

For linear systems, although the trapezoidal rule is an implicit integrator, it can be solved exactly.

.. math::

    f(t, \\boldsymbol{x}) = \\dot{\\boldsymbol{x}} = A(t) \\boldsymbol{x} + \\boldsymbol{b}(t) \\\\
    \\boldsymbol{x}_2 = \\boldsymbol{x}_1 + \\frac12 h (f(t_1, \\boldsymbol{x}_1) + f(t_2, \\boldsymbol{x}_2))

The solution to the trapezoidal formula becomes:

.. math::

    \\left( I - \\frac12 h A(t_2) \\right) \\boldsymbol{x}_2 =
    \\boldsymbol{x}_1 + \\frac12 h (A(t_1) \\boldsymbol{x}_1 + \\boldsymbol{b}_1 + \\boldsymbol{b}_2)

And is simply solved as a linear equation.

:param x1:
    State :math:`x(t)` at :math:`t = t_1`.
:param h:
    Step size.
:param A1:
    Linear coefficient matrix :math:`A(t)` for linear system at :math:`t = t_1`.
:param b1:
    Ordinate vector :math:`\\boldsymbol{b}(t)` for linear system at :math:`t = t_1`.
:param A2:
    Linear coefficient matrix :math:`A(t)` for linear system at :math:`t = t_2`.
:param b2:
    Ordinate vector :math:`\\boldsymbol{b}(t)` for linear system at :math:`t = t_2`.
:return:
    The next step.
"""
function trap(x₁, h, A₁, b₁, A₂, b₂)
    tb = x₁ + 0.5h * (A₁ * x₁ + b₁ + b₂)
    tA = I - 0.5h * A₂
    return tA \ tb
end
