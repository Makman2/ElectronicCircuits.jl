export flatten
flatten(iterator) = reduce(vcat, iterator)
