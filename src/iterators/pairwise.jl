import Base: iterate, length, eltype

export Pairwise
struct Pairwise{IterType}
    iter::IterType
end

function iterate(pairwise::Pairwise)
    next = iterate(pairwise.iter)
    return iterate(pairwise, next)
end

function iterate(pairwise::Pairwise, state)
    previous, previous_original_state = state
    next = iterate(pairwise.iter, previous_original_state)
    return isnothing(next) ? nothing : ((previous, next[1]), next)
end

length(pairwise::Pairwise) = max(0, length(pairwise.iter) - 1)
eltype(::Type{Pairwise{IterType}}) where IterType = eltype(IterType)

export pairwise
pairwise(iter) = Pairwise(iter)
