import Base: reindex


export insort!
function insort!(a, x)
    insert!(a, searchsortedlast(a, x) + 1, x)
end
function insort!(a, x, lo, hi)
    slice = @view a[lo:hi]
    if isempty(slice)
        return push!(a, x)
    end
    idx = searchsortedlast(slice, x)
    insert!(a, idx == 0 ? reindex(slice.indices, (idx + 1,))[1] : reindex(slice.indices, (idx,))[1] + 1, x)
end
