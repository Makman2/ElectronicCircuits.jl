import Base: show


export Unique
"""
Allows to easily create unique objects to be used as unique IDs.
"""
mutable struct Unique end

function show(io::IO, unique::Unique)
    show(io, objectid(unique))
end
