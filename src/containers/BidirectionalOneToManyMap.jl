import Base: copy, delete!, eltype, empty!, get, getindex, haskey, in, isempty, iterate, keys, length, setindex!, show, values


struct ObverseMap{K, V} <: AbstractDict{K, V}
    own::Dict{K, Set{V}}
    reversed::Dict{V, K}
end

get(o::ObverseMap{K, V}, k::K, default) where {K, V} = get(o.own, k, default)
getindex(o::ObverseMap{K, V}, k::K) where {K, V} = getindex(o.own, k)
function setindex!(o::ObverseMap{K, V}, v::V, k::K) where {K, V}
    s = k ∉ keys(o.own) ? o.own[k] = Set{V}() : o.own[k]
    push!(s, v)

    if haskey(o.reversed, v)
        rk = o.reversed[v]
        rs = o.own[rk]
        delete!(rs, v)
        if isempty(rs)
            delete!(o.own, rk)
        end
    end
    o.reversed[v] = k

    return o
end
function delete!(o::ObverseMap{K, V}, k::K) where {K, V}
    if haskey(o.own, k)
        for v in o.own[k]
            delete!(o.reversed, v)
        end
        delete!(o.own, k)
    end
    return o
end
function delete!(o::ObverseMap{K, V}, k::K, v::V) where {K, V}
    if haskey(o.own, k)
        ks = o.own[k]
        delete!(ks, v)
        if isempty(ks)
            delete!(o.own, k)
        end

        delete!(o.reversed, v)
    end
    return o
end
function delete!(o::ObverseMap{K, V}, pair::Pair{K, V}) where {K, V}
    delete!(o, pair.first, pair.second)
end
empty!(o::ObverseMap) = (empty!(o.own); empty!(o.reversed); o)
isempty(o::ObverseMap) = isempty(o.own)
in(p::Pair, o::ObverseMap) = in(p.first, o.own) && in(p.second, o.own[p.first])
haskey(o::ObverseMap{K, V}, key::K) where {K, V} = haskey(o.own, key)
length(o::ObverseMap) = length(o.own)
keys(o::ObverseMap) = keys(o.own)
values(o::ObverseMap) = values(o.own)
iterate(o::ObverseMap) = iterate(o.own)
iterate(o::ObverseMap, state) = iterate(o.own, state)
eltype(::Type{ObverseMap{K, V}}) where {K, V} = eltype(Dict{K, Set{V}})

struct ReverseMap{V, K} <: AbstractDict{V, K}
    own::Dict{V, K}
    reversed::Dict{K, Set{V}}
end

get(r::ReverseMap{V, K}, v::V, default) where {V, K} = get(r.own, v, default)
getindex(r::ReverseMap{V, K}, v::V) where {V, K} = getindex(r.own, v)
function setindex!(r::ReverseMap{V, K}, k::K, v::V) where {V, K}
    if haskey(r.own, v)
        ok = r.own[v]
        os = r.reversed[ok]
        delete!(os, v)
        if isempty(os)
            delete!(r.reversed, ok)
        end
    end
    r.own[v] = k

    s = k ∉ keys(r.reversed) ? r.reversed[k] = Set{V}() : r.reversed[k]
    push!(s, v)

    return r
end
function delete!(r::ReverseMap{V, K}, v::V) where {V, K}
    if haskey(r.own, v)
        ok = r.own[v]
        os = r.reversed[ok]
        delete!(os, v)
        if isempty(os)
            delete!(r.reversed, ok)
        end
        delete!(r.own, v)
    end
    return r
end
empty!(r::ReverseMap) = (empty!(r.own); empty!(r.reversed); o)
isempty(r::ReverseMap) = isempty(r.own)
in(p::Pair, r::ReverseMap) = in(p, r.own)
haskey(r::ReverseMap{V, K}, v::V) where {V, K} = haskey(r.own, v)
length(r::ReverseMap) = length(r.own)
keys(r::ReverseMap) = keys(r.own)
values(r::ReverseMap) = values(r.own)
iterate(r::ReverseMap) = iterate(r.own)
iterate(r::ReverseMap, state) = iterate(r.own, state)
eltype(::Type{ReverseMap{V, K}}) where {V, K} = eltype(Dict{V, K})

export BidirectionalOneToManyMap
struct BidirectionalOneToManyMap{K, V}
    obverse::ObverseMap{K, V}
    reverse::ReverseMap{V, K}

    function BidirectionalOneToManyMap{K, V}() where {K, V}
        obverse = Dict{K, Set{V}}()
        reverse = Dict{V, K}()
        new(ObverseMap(obverse, reverse), ReverseMap(reverse, obverse))
    end

    function BidirectionalOneToManyMap(map::BidirectionalOneToManyMap{K, V}) where {K, V}
        obverse = Dict(k => copy(vs) for (k, vs) in map.obverse.own)
        reverse = copy(map.reverse.own)
        new{K, V}(ObverseMap(obverse, reverse), ReverseMap(reverse, obverse))
    end
end

BidirectionalOneToManyMap() = BidirectionalOneToManyMap{Any, Any}()

function BidirectionalOneToManyMap(pair::Pair{K, V}) where {K, V}
    map = BidirectionalOneToManyMap{K, V}()
    map.obverse[pair.first] = pair.second
    return map
end

function BidirectionalOneToManyMap(pairs::Pair{K, V}...) where {K, V}
    map = BidirectionalOneToManyMap{K, V}()
    for pair in pairs
        map.obverse[pair.first] = pair.second
    end
    return map
end

function BidirectionalOneToManyMap{K, V}(kv) where {K, V}
    map = BidirectionalOneToManyMap{K, V}()
    for (k, v) in kv
        map.obverse[k] = v
    end
    return map
end

copy(m::BidirectionalOneToManyMap) = BidirectionalOneToManyMap(m)
get(m::BidirectionalOneToManyMap{K, V}, k::K, default) where {K, V} = get(m.obverse, k, default)
getindex(m::BidirectionalOneToManyMap{K, V}, k::K) where {K, V} = getindex(m.obverse, k)
setindex!(m::BidirectionalOneToManyMap{K, V}, v::V, k::K) where {K, V} = setindex!(m.obverse, v, k)
delete!(m::BidirectionalOneToManyMap{K, V}, k::K) where {K, V} = delete!(m.obverse, k)
empty!(m::BidirectionalOneToManyMap) = empty!(m.obverse)
isempty(m::BidirectionalOneToManyMap) = isempty(m.obverse)
in(p::Pair, m::BidirectionalOneToManyMap, valcmp=(==)) = in(p, m.obverse, valcmp)
haskey(m::BidirectionalOneToManyMap{K, V}, key::K) where {K, V} = haskey(m.obverse, key)
length(m::BidirectionalOneToManyMap) = length(m.reverse)
keys(m::BidirectionalOneToManyMap) = keys(m.obverse)
values(m::BidirectionalOneToManyMap) = values(m.obverse)
iterate(m::BidirectionalOneToManyMap) = iterate(m.obverse)
iterate(m::BidirectionalOneToManyMap, state) = iterate(m.obverse, state)
eltype(::Type{BidirectionalOneToManyMap{K, V}}) where {K, V} = eltype(ObverseMap{K, V})

function show(io::IO, map::BidirectionalOneToManyMap)
    print(io, "$(typeof(map))(")
    if !isempty(map)
        print(io, join(map.obverse, ","))
        return
    end
    print(io, ")")
end

function show(io::IO, ::MIME"text/plain", map::BidirectionalOneToManyMap)
    if isempty(map)
        print(io, "$(typeof(map))()")
        return
    end
    len = length(map)
    if len == 1
        print(io, "$(typeof(map)) with 1 entry:")
    else
        print(io, "$(typeof(map)) with $len entries:")
    end

    for (k, vs) in map.obverse
        for v in vs
            print(io, "\n  $(Pair(k, v))")
        end
    end
end
