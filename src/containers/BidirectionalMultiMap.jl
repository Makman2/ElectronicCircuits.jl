import Base: copy, delete!, eltype, empty!, get, getindex, haskey, in, isempty, iterate, keys, length, setindex!, show, values


struct OnewaySubMultiMap{K, V} <: AbstractDict{K, V}
    own::Dict{K, Set{V}}
    reversed::Dict{V, Set{K}}
end

get(d::OnewaySubMultiMap{K, V}, k::K, default) where {K, V} = get(d.own, k, default)
getindex(d::OnewaySubMultiMap{K, V}, k::K) where {K, V} = getindex(d.own, k)
function setindex!(d::OnewaySubMultiMap{K, V}, v::V, k::K) where {K, V}
    s = k ∉ keys(d.own) ? d.own[k] = Set{V}() : d.own[k]
    push!(s, v)
    s = v ∉ keys(d.reversed) ? d.reversed[v] = Set{K}() : d.reversed[v]
    push!(s, k)
    return d
end
function delete!(d::OnewaySubMultiMap{K, V}, k::K) where {K, V}
    if haskey(d.own, k)
        for v in d.own[k]
            vs = d.reversed[v]
            delete!(vs, k)
            if isempty(vs)
                delete!(d.reversed, v)
            end
        end
        delete!(d.own, k)
    end
    return d
end
function delete!(d::OnewaySubMultiMap{K, V}, k::K, v::V) where {K, V}
    if haskey(d.own, k)
        ks = d.own[k]
        delete!(ks, v)
        if isempty(ks)
            delete!(d.own, k)
        end

        vs = d.reversed[v]
        delete!(vs, k)
        if isempty(vs)
            delete!(d.reversed, v)
        end
    end
    return d
end
function delete!(d::OnewaySubMultiMap{K, V}, pair::Pair{K, V}) where {K, V}
    delete!(d, pair.first, pair.second)
end
empty!(d::OnewaySubMultiMap) = (empty!(d.own); empty!(d.reversed); d)
isempty(d::OnewaySubMultiMap) = isempty(d.own)
in(p::Pair, d::OnewaySubMultiMap) = in(p.first, d.own) && in(p.second, d.own[p.first])
haskey(d::OnewaySubMultiMap{K, V}, key::K) where {K, V} = haskey(d.own, key)
length(d::OnewaySubMultiMap) = length(d.own)
keys(d::OnewaySubMultiMap) = keys(d.own)
values(d::OnewaySubMultiMap) = values(d.own)
iterate(d::OnewaySubMultiMap) = iterate(d.own)
iterate(d::OnewaySubMultiMap, state) = iterate(d.own, state)
eltype(::Type{OnewaySubMultiMap{K, V}}) where {K, V} = eltype(Dict{K, Set{V}})

export BidirectionalMultiMap
struct BidirectionalMultiMap{K, V}
    obverse::OnewaySubMultiMap{K, V}
    reverse::OnewaySubMultiMap{V, K}

    function BidirectionalMultiMap{K, V}() where {K, V}
        obverse = Dict{K, Set{V}}()
        reverse = Dict{V, Set{K}}()
        new(OnewaySubMultiMap(obverse, reverse), OnewaySubMultiMap(reverse, obverse))
    end

    function BidirectionalMultiMap(map::BidirectionalMultiMap{K, V}) where {K, V}
        obverse = Dict(k => copy(vs) for (k, vs) in map.obverse.own)
        reverse = Dict(v => copy(ks) for (v, ks) in map.reverse.own)
        new{K, V}(OnewaySubMultiMap(obverse, reverse), OnewaySubMultiMap(reverse, obverse))
    end
end

BidirectionalMultiMap() = BidirectionalMultiMap{Any, Any}()

function BidirectionalMultiMap(pair::Pair{K, V}) where {K, V}
    map = BidirectionalMultiMap{K, V}()
    map.obverse[pair.first] = pair.second
    return map
end

function BidirectionalMultiMap(pairs::Pair{K, V}...) where {K, V}
    map = BidirectionalMultiMap{K, V}()
    for pair in pairs
        map.obverse[pair.first] = pair.second
    end
    return map
end

function BidirectionalMultiMap{K, V}(kv) where {K, V}
    map = BidirectionalMultiMap{K, V}()
    for (k, v) in kv
        map.obverse[k] = v
    end
    return map
end

copy(m::BidirectionalMultiMap) = BidirectionalMultiMap(m)
get(m::BidirectionalMultiMap{K, V}, k::K, default) where {K, V} = get(m.obverse, k, default)
getindex(m::BidirectionalMultiMap{K, V}, k::K) where {K, V} = getindex(m.obverse, k)
setindex!(m::BidirectionalMultiMap{K, V}, v::V, k::K) where {K, V} = setindex!(m.obverse, v, k)
delete!(m::BidirectionalMultiMap{K, V}, k::K) where {K, V} = delete!(m.obverse, k)
empty!(m::BidirectionalMultiMap) = empty!(m.obverse)
isempty(m::BidirectionalMultiMap) = isempty(m.obverse)
in(p::Pair, m::BidirectionalMultiMap, valcmp=(==)) = in(p, m.obverse, valcmp)
haskey(m::BidirectionalMultiMap{K, V}, key::K) where {K, V} = haskey(m.obverse, key)
length(m::BidirectionalMultiMap) = sum(length(s) for s in values(m.obverse))
keys(m::BidirectionalMultiMap) = keys(m.obverse)
values(m::BidirectionalMultiMap) = values(m.obverse)
iterate(m::BidirectionalMultiMap) = iterate(m.obverse)
iterate(m::BidirectionalMultiMap, state) = iterate(m.obverse, state)
eltype(::Type{BidirectionalMultiMap{K, V}}) where {K, V} = eltype(OnewaySubMultiMap{K, V})

function show(io::IO, map::BidirectionalMultiMap)
    print(io, "$(typeof(map))(")
    if !isempty(map)
        print(io, join(map.obverse, ","))
        return
    end
    print(io, ")")
end

function show(io::IO, ::MIME"text/plain", map::BidirectionalMultiMap)
    if isempty(map)
        print(io, "$(typeof(map))()")
        return
    end
    len = length(map)
    if len == 1
        print(io, "$(typeof(map)) with 1 entry:")
    else
        print(io, "$(typeof(map)) with $len entries:")
    end

    for (k, vs) in map.obverse
        for v in vs
            print(io, "\n  $(Pair(k, v))")
        end
    end
end
