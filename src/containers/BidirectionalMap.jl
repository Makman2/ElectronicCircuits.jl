import Base: copy, delete!, eltype, empty!, get, getindex, haskey, in, isempty, iterate, keys, length, setindex!, show, values


struct OnewaySubMap{K, V} <: AbstractDict{K, V}
    own::Dict{K, V}
    reversed::Dict{V, K}
end

get(d::OnewaySubMap{K, V}, k::K, default) where {K, V} = get(d.own, k, default)
getindex(d::OnewaySubMap{K, V}, k::K) where {K, V} = getindex(d.own, k)
function setindex!(d::OnewaySubMap{K, V}, v::V, k::K) where {K, V}
    if haskey(d.own, k)
        delete!(d.reversed, d.own[k])
    end
    if haskey(d.reversed, v)
        delete!(d.own, d.reversed[v])
    end
    d.own[k] = v
    d.reversed[v] = k
    return d
end
function delete!(d::OnewaySubMap{K, V}, k::K) where {K, V}
    if haskey(d.own, k)
        v = d.own[k]
        delete!(d.own, k)
        delete!(d.reversed, v)
    end
    return d
end
empty!(d::OnewaySubMap) = (empty!(d.own); empty!(d.reversed); d)
isempty(d::OnewaySubMap) = isempty(d.own)
in(p::Pair, d::OnewaySubMap, valcmp=(==)) = in(p, d.own, valcmp)
haskey(d::OnewaySubMap{K, V}, key::K) where {K, V} = haskey(d.own, key)
length(d::OnewaySubMap) = length(d.own)
keys(d::OnewaySubMap) = keys(d.own)
values(d::OnewaySubMap) = values(d.own)
iterate(d::OnewaySubMap) = iterate(d.own)
iterate(d::OnewaySubMap, state) = iterate(d.own, state)
eltype(::Type{OnewaySubMap{K, V}}) where {K, V} = eltype(Dict{K, V})

export BidirectionalMap
struct BidirectionalMap{K, V}
    obverse::OnewaySubMap{K, V}
    reverse::OnewaySubMap{V, K}

    function BidirectionalMap{K, V}() where {K, V}
        obverse = Dict{K, V}()
        reverse = Dict{V, K}()
        new(OnewaySubMap(obverse, reverse), OnewaySubMap(reverse, obverse))
    end

    function BidirectionalMap(map::BidirectionalMap{K, V}) where {K, V}
        obverse = copy(map.obverse.own)
        reverse = copy(map.reverse.own)
        new{K, V}(OnewaySubMap(obverse, reverse), OnewaySubMap(reverse, obverse))
    end
end

BidirectionalMap() = BidirectionalMap{Any, Any}()

function BidirectionalMap(pair::Pair{K, V}) where {K, V}
    map = BidirectionalMap{K, V}()
    map.obverse[pair.first] = pair.second
    return map
end

function BidirectionalMap(pairs::Pair{K, V}...) where {K, V}
    map = BidirectionalMap{K, V}()
    for pair in pairs
        map.obverse[pair.first] = pair.second
    end
    return map
end

function BidirectionalMap{K, V}(kv) where {K, V}
    map = BidirectionalMap{K, V}()
    for (k, v) in kv
        map.obverse[k] = v
    end
    return map
end

copy(m::BidirectionalMap) = BidirectionalMap(m)
get(m::BidirectionalMap{K, V}, k::K, default) where {K, V} = get(m.obverse, k, default)
getindex(m::BidirectionalMap{K, V}, k::K) where {K, V} = getindex(m.obverse, k)
setindex!(m::BidirectionalMap{K, V}, v::V, k::K) where {K, V} = setindex!(m.obverse, v, k)
delete!(m::BidirectionalMap{K, V}, k::K) where {K, V} = delete!(m.obverse, k)
empty!(m::BidirectionalMap) = empty!(m.obverse)
isempty(m::BidirectionalMap) = isempty(m.obverse)
in(p::Pair, m::BidirectionalMap, valcmp=(==)) = in(p, m.obverse, valcmp)
haskey(m::BidirectionalMap{K, V}, key::K) where {K, V} = haskey(m.obverse, key)
length(m::BidirectionalMap) = length(m.obverse)
keys(m::BidirectionalMap) = keys(m.obverse)
values(m::BidirectionalMap) = values(m.obverse)
iterate(m::BidirectionalMap) = iterate(m.obverse)
iterate(m::BidirectionalMap, state) = iterate(m.obverse, state)
eltype(::Type{BidirectionalMap{K, V}}) where {K, V} = eltype(OnewaySubMap{K, V})

function show(io::IO, map::BidirectionalMap)
    print(io, "$(typeof(map))(")
    if !isempty(map)
        print(io, join(map.obverse, ","))
        return
    end
    print(io, ")")
end


function show(io::IO, ::MIME"text/plain", map::BidirectionalMap)
    if isempty(map)
        print(io, "$(typeof(map))()")
        return
    end
    len = length(map)
    if len == 1
        print(io, "$(typeof(map)) with 1 entry:")
    else
        print(io, "$(typeof(map)) with $len entries:")
    end

    for pair in map.obverse
        print(io, "\n  $pair")
    end
end
