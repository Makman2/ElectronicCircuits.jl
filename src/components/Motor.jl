import Base: show


mutable struct MotorState
    voltage::Float64
    current::Float64
    ω::Float64
end

struct MotorPins
    neg::Pin
    pos::Pin

    MotorPins(component::Component) = new(Pin(component), Pin(component))
end

export Motor
mutable struct Motor <: Component
    R::Float64
    L::Float64
    Kₜ::Float64
    Kₑ::Float64
    J::Float64
    D::Float64
    Tₗ::Function

    state::MotorState

    pins::MotorPins

    Motor(
        R::Float64 = 0.425,
        L::Float64 = 0.00378,
        Kₜ::Float64 = 0.466,
        Kₑ::Float64 = 0.466,
        J::Float64 = 0.02255,
        D::Float64 = 0.001,
        Tₗ::Function = zero,
        stateᵥ::Float64 = 0.0,
        stateᵢ::Float64 = 0.0,
        stateₒ::Float64 = 0.0) = (x = new(R, L, Kₜ, Kₑ, J, D, Tₗ, MotorState(stateᵥ, stateᵢ, stateₒ)); x.pins = MotorPins(x); x)
end

export statespace
statespace(motor::Motor, t::Float64, v::Float64) = (
    # Linear dynamic state-space vector: x = [i, ω]
    [(-motor.R / motor.L) (-motor.Kₑ / motor.L);
     (motor.Kₜ / motor.J) (-motor.D / motor.J)],
    [v / motor.L, -motor.Tₗ(t) / motor.J]
)

function nextstate(motor::Motor, x::Vector{Float64}, t₁::Float64, t₂::Float64)::Vector{Float64}
    x₁ = [motor.state.current, motor.state.ω]
    A₁, b₁ = statespace(motor, t₁, motor.state.voltage)
    A₂, b₂ = statespace(motor, t₂, x[1])
    return trap(x₁, t₂ - t₁, A₁, b₁, A₂, b₂)
end

output(motor::Motor, x::Vector{Float64}, t₁::Float64, t₂::Float64)::Vector{Float64} = return nextstate(motor, x, t₁, t₂)[1:1]
branches(motor::Motor, pinmap::AbstractDict{Pin, Node})::Array{Branch, 1} = [CurrentBranch(pinmap[motor.pins.neg], pinmap[motor.pins.pos])]

function update!(motor::Motor, x::Vector{Float64}, t₁::Float64, t₂::Float64)
    i, ω = nextstate(motor, x, t₁, t₂)
    motor.state.voltage = x[1]
    motor.state.current = i
    motor.state.ω = ω
end

get_voltages(sim::SimulationResult, motor::Motor)::Vector{Float64} = get_voltages(sim, motor.pins.neg, motor.pins.pos)
get_currents(sim::SimulationResult, motor::Motor)::Vector{Float64} = get_states(sim, motor, 2)

get_state(motor::Motor)::Vector{Float64} = [motor.state.voltage, motor.state.current, motor.state.ω]
function set_state!(motor::Motor, state::Vector{Float64})
    motor.state.voltage, motor.state.current, motor.state.ω = state
end

function show(io::IO, state::MotorState)
    print(io, "($(state.voltage)V, $(state.current)A, $(state.ω)rad/s)")
end

function show(io::IO, motor::Motor)
    print(io, "$(typeof(motor))(R=$(motor.R)Ω, L=$(motor.L)H, Kₜ=$(motor.Kₜ)Nm/A, Kₑ=$(motor.Kₑ)V⋅rad/s, J=$(motor.J)kg⋅m², D=$(motor.D)Nm, Tₗ(t)=$(motor.Tₗ)(t) [Nm], state=$(motor.state))")
end
