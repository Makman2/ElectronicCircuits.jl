import Base: show


struct ConstantVoltageSourcePins
    neg::Pin
    pos::Pin

    ConstantVoltageSourcePins(component::Component) = new(Pin(component), Pin(component))
end

export ConstantVoltageSource
mutable struct ConstantVoltageSource <: Component
    value::Float64
    pins::ConstantVoltageSourcePins

    ConstantVoltageSource(value::Float64) = (x = new(value); x.pins = ConstantVoltageSourcePins(x); x)
end

output(source::ConstantVoltageSource, x::Vector{Float64}, t₁::Float64, t₂::Float64)::Vector{Float64} = [source.value]
branches(source::ConstantVoltageSource, pinmap::AbstractDict{Pin, Node})::Array{Branch, 1} = [VoltageBranch(pinmap[source.pins.neg], pinmap[source.pins.pos])]

get_voltages(sim::SimulationResult, source::ConstantVoltageSource)::Vector{Float64} = get_voltages(sim, source.pins.neg, source.pins.pos)
get_currents(sim::SimulationResult, source::ConstantVoltageSource)::Vector{Float64} = get_currents(sim, source, 1)

function show(io::IO, source::ConstantVoltageSource)
    print(io, "$(typeof(source))($(source.value)V)")
end
