import Base: show


struct DiodePins
    anode::Pin
    cathode::Pin

    DiodePins(component::Component) = new(Pin(component), Pin(component))
end

export Diode
mutable struct Diode <: Component
    Iₛ::Float64
    Vₜ::Float64
    n::Float64
    pins::DiodePins

    Diode(Iₛ::Float64 = 1e-12, Vₜ::Float64 = 0.02585, n::Float64 = 1.0) = (x = new(Iₛ, Vₜ, n); x.pins = DiodePins(x); x)
end

output(d::Diode, x::Vector{Float64}, t₁::Float64, t₂::Float64)::Vector{Float64} = output(d, x)
output(d::Diode, x::Vector{Float64})::Vector{Float64} = @. d.Iₛ * (exp(x / (d.n * d.Vₜ)) - 1.0)
branches(d::Diode, pinmap::AbstractDict{Pin, Node})::Array{Branch, 1} = [CurrentBranch(pinmap[d.pins.cathode], pinmap[d.pins.anode])]

get_voltages(sim::SimulationResult, d::Diode)::Vector{Float64} = get_voltages(sim, d.pins.cathode, d.pins.anode)
#get_currents(sim::SimulationResult, d::Diode)::Vector{Float64} = get_voltages(sim, d.pins.cathode, d.pins.anode) ./ r.value  # TODO Maybe I should store currents in results as well...

function show(io::IO, d::Diode)
    print(io, "$(typeof(d))(Iₛ=$(d.Iₛ)A, Vₜ=$(d.Vₜ)V, n=$(d.n))")
end
