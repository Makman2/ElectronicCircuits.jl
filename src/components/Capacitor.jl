import Base: show


mutable struct CapacitorState
    voltage::Float64
    current::Float64  # Current is stored too, because trapezoidal type numerical integration is performed.
end

struct CapacitorPins
    a::Pin
    b::Pin

    CapacitorPins(component::Component) = new(Pin(component), Pin(component))
end

export Capacitor
mutable struct Capacitor <: Component
    value::Float64
    state::CapacitorState
    pins::CapacitorPins

    Capacitor(value::Float64, stateᵥ::Float64 = 0.0, stateᵢ::Float64 = 0.0) = (x = new(value, CapacitorState(stateᵥ, stateᵢ)); x.pins = CapacitorPins(x); x)
end

output(c::Capacitor, x::Vector{Float64}, t₁::Float64, t₂::Float64)::Vector{Float64} = @. c.state.voltage + 0.5 * (t₂ - t₁) * (c.state.current + x) / c.value
branches(c::Capacitor, pinmap::AbstractDict{Pin, Node})::Array{Branch, 1} = [VoltageBranch(pinmap[c.pins.a], pinmap[c.pins.b])]

function update!(c::Capacitor, x::Vector{Float64}, t₁::Float64, t₂::Float64)
    c.state.voltage = output(c, x, t₁, t₂)[1]  # Evaluation of output must happen before setting current state!
    c.state.current = x[1]
end

get_state(c::Capacitor)::Vector{Float64} = [c.state.voltage, c.state.current]
function set_state!(c::Capacitor, state::Vector{Float64})
    c.state.voltage, c.state.current = state
end

get_voltages(sim::SimulationResult, c::Capacitor)::Vector{Float64} = get_voltages(sim, c.pins.a, c.pins.b)
get_currents(sim::SimulationResult, c::Capacitor)::Vector{Float64} = get_currents(sim, c, 1)

function show(io::IO, state::CapacitorState)
    print(io, "($(state.voltage)V, $(state.current)A)")
end

function show(io::IO, c::Capacitor)
    print(io, "$(typeof(c))($(c.value)F, state=$(c.state))")
end
