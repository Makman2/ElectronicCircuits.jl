import Base: show


mutable struct InductorState
    voltage::Float64  # Voltage is stored too, because trapezoidal type numerical integration is performed.
    current::Float64
end

struct InductorPins
    a::Pin
    b::Pin

    InductorPins(component::Component) = new(Pin(component), Pin(component))
end

export Inductor
mutable struct Inductor <: Component
    value::Float64
    state::InductorState
    pins::InductorPins

    Inductor(value::Float64, stateᵥ::Float64 = 0.0, stateᵢ::Float64 = 0.0) = (x = new(value, InductorState(stateᵥ, stateᵢ)); x.pins = InductorPins(x); x)
end

output(l::Inductor, x::Vector{Float64}, t₁::Float64, t₂::Float64)::Vector{Float64} = @. l.state.current + 0.5 * (t₂ - t₁) * (l.state.voltage + x) / l.value
branches(l::Inductor, pinmap::AbstractDict{Pin, Node})::Array{Branch, 1} = [CurrentBranch(pinmap[l.pins.a], pinmap[l.pins.b])]

function update!(l::Inductor, x::Vector{Float64}, t₁::Float64, t₂::Float64)
    l.state.current = output(l, x, t₁, t₂)[1]  # Evaluation of output must happen before setting voltage state!
    l.state.voltage = x[1]
end

get_state(l::Inductor)::Vector{Float64} = [l.state.voltage, l.state.current]
function set_state!(l::Inductor, state::Vector{Float64})
    l.state.voltage, l.state.current = state
end

get_voltages(sim::SimulationResult, l::Inductor)::Vector{Float64} = get_voltages(sim, l.pins.a, l.pins.b)
get_currents(sim::SimulationResult, l::Inductor)::Vector{Float64} = get_states(sim, l, 2)

function show(io::IO, state::InductorState)
    print(io, "($(state.voltage)V, $(state.current)A)")
end

function show(io::IO, l::Inductor)
    print(io, "$(typeof(l))($(l.value)F, state=$(l.state))")
end
