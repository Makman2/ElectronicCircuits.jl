import Base: show


struct BipolarJunctionTransistorPins
    base::Pin
    collector::Pin
    emitter::Pin

    BipolarJunctionTransistorPins(component::Component) = new(Pin(component), Pin(component), Pin(component))
end

export BipolarJunctionTransistor
mutable struct BipolarJunctionTransistor <: Component
    Iₛ::Float64
    βF::Float64
    βR::Float64
    Vₜ::Float64

    pins::BipolarJunctionTransistorPins

    BipolarJunctionTransistor(
        Iₛ::Float64 = 1e-12,
        βF::Float64 = 260.0,
        βR::Float64 = 10.0,
        Vₜ::Float64 = 0.02585) = (x = new(Iₛ, βF, βR, Vₜ); x.pins = BipolarJunctionTransistorPins(x); x)
end

branches(transistor::BipolarJunctionTransistor, pinmap::AbstractDict{Pin, Node})::Array{Branch, 1} = [
    CurrentBranch(pinmap[component.pins.base], pinmap[component.pins.collector]),  # BC (base-collector) branch
    CurrentBranch(pinmap[component.pins.base], pinmap[component.pins.emitter]),  # BE (base-emitter) branch
]
output(transistor::BipolarJunctionTransistor, x::Vector{Float64}, t₁::Float64, t₂::Float64)::Vector{Float64} = output(transistor, x)
function output(transistor::BipolarJunctionTransistor, x::Vector{Float64})::Vector{Float64}
    exp_bc_term, exp_be_term = @. exp(x / transistor.Vₜ)
    return transistor.Iₛ * [
        exp_be_term - exp_bc_term - (exp_bc_term - 1.0) / transistor.βR,
        exp_be_term - exp_bc_term + (exp_be_term - 1.0) / transistor.βF,
    ]
end

function show(io::IO, transistor::BipolarJunctionTransistor)
    print(io, "$(typeof(transistor))(Iₛ=$(transistor.Iₛ)A, βF=$(transistor.βF), βR=$(transistor.βR), Vₜ=$(transistor.Vₜ)V)")
end
