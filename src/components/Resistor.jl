import Base: show


struct ResistorPins
    a::Pin
    b::Pin

    ResistorPins(component::Component) = new(Pin(component), Pin(component))
end

export Resistor
mutable struct Resistor <: Component
    value::Float64
    pins::ResistorPins

    Resistor(value::Float64) = (x = new(value); x.pins = ResistorPins(x); x)
end

output(r::Resistor, x::Vector{Float64}, t₁::Float64, t₂::Float64)::Vector{Float64} = output(r, x)
output(r::Resistor, x::Vector{Float64})::Vector{Float64} = x ./ r.value
branches(r::Resistor, pinmap::AbstractDict{Pin, Node})::Array{Branch, 1} = [CurrentBranch(pinmap[r.pins.a], pinmap[r.pins.b])]

get_voltages(sim::SimulationResult, r::Resistor)::Vector{Float64} = get_voltages(sim, r.pins.a, r.pins.b)
get_currents(sim::SimulationResult, r::Resistor)::Vector{Float64} = get_voltages(sim, r.pins.a, r.pins.b) ./ r.value

function show(io::IO, r::Resistor)
    print(io, "$(typeof(r))($(r.value)Ω)")
end
