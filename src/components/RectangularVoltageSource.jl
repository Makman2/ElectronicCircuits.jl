import Base: show


struct RectangularVoltageSourcePins
    neg::Pin
    pos::Pin

    RectangularVoltageSourcePins(component::Component) = new(Pin(component), Pin(component))
end

export RectangularVoltageSource
mutable struct RectangularVoltageSource <: Component
    frequency::Float64
    amplitude::Float64
    phase::Float64
    offset::Float64
    duty::Float64

    pins::RectangularVoltageSourcePins

    RectangularVoltageSource(frequency::Float64=50.0, amplitude::Float64=325.0, phase::Float64=0.0, offset::Float64=0.0, duty::Float64=0.5) = (x = new(frequency, amplitude, phase, offset, duty); x.pins = RectangularVoltageSourcePins(x); x)
end

function output(source::RectangularVoltageSource, x::Vector{Float64}, t₁::Float64, t₂::Float64)::Vector{Float64}
    high = (2π * source.frequency * t₂ + source.phase) % (2π) < 2π * source.duty
    return [source.amplitude * (high ? 1.0 : -1.0) + source.offset]
end
branches(source::RectangularVoltageSource, pinmap::AbstractDict{Pin, Node})::Array{Branch, 1} = [VoltageBranch(pinmap[source.pins.neg], pinmap[source.pins.pos])]

get_voltages(sim::SimulationResult, source::RectangularVoltageSource)::Vector{Float64} = get_voltages(sim, source.pins.neg, source.pins.pos)
get_currents(sim::SimulationResult, source::RectangularVoltageSource)::Vector{Float64} = get_currents(sim, source, 1)

function show(io::IO, source::RectangularVoltageSource)
    print(io, "$(typeof(source))($(source.frequency)Hz, $(source.offset - source.amplitude)V↔$(source.offset + source.amplitude)V, D=$(source.duty), ϕ=$(source.phase)rad)")
end

phaseinterval_to_time(source::RectangularVoltageSource, phaseinterval::Float64)::Float64 = (2π * phaseinterval - source.phase) / (2π * source.frequency)

function next_event(source::RectangularVoltageSource, t₁::Float64, t₂::Float64)::Union{Float64, Nothing}
    n₁, ϕ₁ = divrem(2π * source.frequency * t₁ + source.phase, 2π)
    n₂, ϕ₂ = divrem(2π * source.frequency * t₂ + source.phase, 2π)

    if ϕ₁ == 0.0
        return phaseinterval_to_time(source, n₁)
    elseif n₂ > n₁
        if ϕ₁ ≤ 2π * source.duty < 2π
            return phaseinterval_to_time(source, n₁ + source.duty)
        else
            return phaseinterval_to_time(source, n₁ + 1)
        end
    else
        if ϕ₁ ≤ 2π * source.duty ≤ ϕ₂
            return phaseinterval_to_time(source, n₁ + source.duty)
        end
    end
    return nothing
end
