import Base: show


struct ConstantCurrentSourcePins
    in::Pin
    out::Pin

    ConstantCurrentSourcePins(component::Component) = new(Pin(component), Pin(component))
end

export ConstantCurrentSource
mutable struct ConstantCurrentSource <: Component
    value::Float64
    pins::ConstantCurrentSourcePins

    ConstantCurrentSource(value::Float64) = (x = new(value); x.pins = ConstantCurrentSourcePins(x); x)
end

output(source::ConstantCurrentSource, x::Vector{Float64}, t₁::Float64, t₂::Float64)::Vector{Float64} = [source.value]
branches(source::ConstantCurrentSource, pinmap::AbstractDict{Pin, Node})::Array{Branch, 1} = [CurrentBranch(pinmap[source.pins.in], pinmap[source.pins.out])]

get_voltages(sim::SimulationResult, source::ConstantCurrentSource)::Vector{Float64} = get_voltages(sim, source.pins.in, source.pins.out)
get_currents(sim::SimulationResult, source::ConstantCurrentSource)::Vector{Float64} = source.value

function show(io::IO, source::ConstantCurrentSource)
    print(io, "$(typeof(source))($(source.value)A)")
end
