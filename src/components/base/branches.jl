import Base: show


export CurrentBranch
struct CurrentBranch
    source::Node
    target::Node
end

function show(io::IO, branch::CurrentBranch)
    print(io, "$(typeof(branch).name)(")
    show(io, branch.source)
    print(io, " → ")
    show(io, branch.target)
    print(io, ")")
end


export VoltageBranch
struct VoltageBranch
    source::Node
    target::Node
end

function show(io::IO, branch::VoltageBranch)
    print(io, "$(typeof(branch).name)(")
    show(io, branch.source)
    print(io, " → ")
    show(io, branch.target)
    print(io, ")")
end


export Branch
const Branch = Union{VoltageBranch, CurrentBranch}
