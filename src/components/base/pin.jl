import Base: show


export Pin
struct Pin
    id::Unique
    component::Component

    Pin(component::Component) = new(Unique(), component)
end

function show(io::IO, pin::Pin)
    print(io, "$(typeof(pin).name)@$(pin.id)($(pin.component))")
end
