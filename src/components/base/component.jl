abstract type Component end

export output
function output end
export branches
function branches end
export next_event
next_event(component::Component, t₁::Float64, t₂::Float64)::Union{Float64, Nothing} = nothing
export update!
update!(component::Component, x::Vector{Float64}, t₁::Float64, t₂::Float64) = nothing
export get_state
get_state(component::Component)::Vector{Float64} = []
export set_state!
set_state!(component::Component, state::Vector{Float64}) = nothing
