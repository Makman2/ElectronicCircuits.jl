import Base: show


export Node
mutable struct Node end

function show(io::IO, node::Node)
    print(io, "Node($(objectid(node)))")
end
