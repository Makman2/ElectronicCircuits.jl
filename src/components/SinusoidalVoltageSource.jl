import Base: show


struct SinusoidalVoltageSourcePins
    neg::Pin
    pos::Pin

    SinusoidalVoltageSourcePins(component::Component) = new(Pin(component), Pin(component))
end

export SinusoidalVoltageSource
mutable struct SinusoidalVoltageSource <: Component
    frequency::Float64
    amplitude::Float64
    phase::Float64

    pins::SinusoidalVoltageSourcePins

    SinusoidalVoltageSource(frequency::Float64=50.0, amplitude::Float64=325.0, phase::Float64=0.0) = (x = new(frequency, amplitude, phase); x.pins = SinusoidalVoltageSourcePins(x); x)
end

effective_amplitude(source::SinusoidalVoltageSource) = source.amplitude / √2
effective_amplitude(source::SinusoidalVoltageSource, rms::Float64) = source.amplitude = rms * √2

output(source::SinusoidalVoltageSource, x::Vector{Float64}, t₁::Float64, t₂::Float64)::Vector{Float64} = [source.amplitude * sin(2π * source.frequency * t₂ + source.phase)]
branches(source::SinusoidalVoltageSource, pinmap::AbstractDict{Pin, Node})::Array{Branch, 1} = [VoltageBranch(pinmap[source.pins.neg], pinmap[source.pins.pos])]

get_voltages(sim::SimulationResult, source::SinusoidalVoltageSource)::Vector{Float64} = get_voltages(sim, source.pins.neg, source.pins.pos)
get_currents(sim::SimulationResult, source::SinusoidalVoltageSource)::Vector{Float64} = get_currents(sim, source, 1)

function show(io::IO, source::SinusoidalVoltageSource)
    print(io, "$(typeof(source))($(source.frequency)Hz, $(source.amplitude)V, ϕ=$(source.phase)rad)")
end
