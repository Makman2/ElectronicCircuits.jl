module ElectronicCircuits

# Order is important!

include("misc/sorted.jl")
include("misc/Unique.jl")
include("iterators/flatten.jl")
include("iterators/pairwise.jl")
include("containers/BidirectionalMap.jl")
include("containers/BidirectionalMultiMap.jl")
include("containers/BidirectionalOneToManyMap.jl")
include("graphs/MultiDiGraph.jl")
include("math/statespace.jl")

include("components/base/node.jl")
include("components/base/branches.jl")
include("components/base/component.jl")
include("components/base/pin.jl")

include("analysis/Circuit.jl")
include("analysis/SimulationResult.jl")
include("analysis/CircuitModel.jl")

include("components/BipolarJunctionTransistor.jl")
include("components/Capacitor.jl")
include("components/ConstantCurrentSource.jl")
include("components/ConstantVoltageSource.jl")
include("components/Diode.jl")
include("components/Inductor.jl")
include("components/Motor.jl")
include("components/RectangularVoltageSource.jl")
include("components/Resistor.jl")
include("components/SinusoidalVoltageSource.jl")

end
