import Base.Iterators: repeated
using NLsolve

# ERP = Event Resolution Precision
export ERP
const ERP = eps(Float64) * (2 << 20)

struct CompiledCircuitModelFunctions
    unpack_system_vector::Function
    couplings::Function
    residual::Function
end

export CircuitModel
struct CircuitModel
    systemsize::Int
    components::Tuple{Vararg{Component}}
    pins_to_system_index::Dict{Pin, Int}
    voltage_branch_coupling_index_to_system_index::Dict{Tuple{Component, Int}, Int}
    state_partitioning::Vector{Int}  # For more convenience, store the state partitioning for the statevector already at construction.
    functions::CompiledCircuitModelFunctions
end

export compile
function compile(circuit::Circuit)::CircuitModel
    # Various relational dictionaries are required to quickly lookup components and their properties.

    # Collect components uniquely to avoid duplication and
    # to preserve components order for calculation later.
    components = collect(Set(pin.component for pin in keys(circuit.connections.reverse)))
    components_to_index = Dict(component => i for (i, component) in enumerate(components))

    # Statevector partitioning already pre-initialized for more convenience when used in simulation methods based on state (e.g. EFM).
    partitioning = [length(get_state(component)) for component in components]

    # The pinmap is passed to components that provide their characteristic branches.
    pinmap = circuit.connections.reverse

    component_to_branches = Dict{Component, Vector{Branch}}()  # Note that branch order is important!
    branches_to_component = Dict{Branch, Component}()  # Map to quickly obtain the associated component to a branch.

    branch_to_coupling_index = Dict{Branch, Int}()
    network = MultiDiGraph{Node, Branch}()
    for component in components
        bs = branches(component, pinmap)

        # TODO Insert unknown nodes into network graph returned by "branches".
        #   See https://gitlab.com/Makman2/ElectronicCircuits.jl/-/issues/1

        for (i, branch) in enumerate(bs)
            branches_to_component[branch] = component
            branch_to_coupling_index[branch] = i
            add_edge!(network, branch.source => branch.target, branch)
        end
        component_to_branches[component] = bs
    end

    # Ground nodes (GND) are not in this "significant node list".
    # If a node used later on inside residual calculation is not inside here,
    # it means that it is a ground node.
    # The ground node is chosen by being randomly popped out of the weakly
    # connected node set. All other nodes will be used for the MNA (Modified Nodal Analysis).
    significant_nodes = Vector{Node}()
    for subset in weakly_connected_components(network)
        pop!(subset)
        append!(significant_nodes, subset)
    end
    significant_nodes_to_system_vector_index = Dict(node => i for (i, node) in enumerate(significant_nodes))
    # Reliable order for additional current variables introduced by voltage branches.
    voltage_branches = collect(branch for branch in keys(branches_to_component) if isa(branch, VoltageBranch))
    voltage_branches_to_system_vector_index = Dict(branch => i for (i, branch) in enumerate(voltage_branches))

    # ⎡v₁⎤  ⎧
    # ⎢⋮ ⎥  ⎨ potentials/voltages of significant nodes (i.e. all non GND nodes)
    # ⎣vₘ⎦  ⎩
    # ⎡i₁⎤  ⎧
    # ⎢⋮ ⎥  ⎨ current variables introduced by voltage branches
    # ⎣iₙ⎦  ⎩

    component_types = [typeof(component) for component in components]

    unpack_system_vector = @eval begin
        $(gensym("unpack_system_vector"))(x::Vector{Float64})::Tuple{Vector{Float64}, Vector{Float64}} = begin
            return (x[1:$(length(significant_nodes))], x[$(length(significant_nodes)+1):end])
        end
    end

    couplings_qualifier = gensym("couplings")
    couplings = @eval begin
        $couplings_qualifier(v::Vector{Float64}, i::Vector{Float64}) = begin
            return [
                $(:($(begin
                    coupling_vector_exprs = []
                    for component in components
                        subexprs = []
                        for branch in component_to_branches[component]
                            if isa(branch, CurrentBranch)
                                expr = nothing
                                if haskey(significant_nodes_to_system_vector_index, branch.target)
                                    expr = :(v[$(significant_nodes_to_system_vector_index[branch.target])])
                                end
                                if haskey(significant_nodes_to_system_vector_index, branch.source)
                                    subexpr = :(v[$(significant_nodes_to_system_vector_index[branch.source])])
                                    expr = isnothing(expr) ? :(-$subexpr) : :($expr - $subexpr)
                                end
                                if isnothing(expr)
                                    expr = :(0)
                                end
                                push!(subexprs, expr)
                            else  # isa(branch, VoltageBranch)
                                expr = :(i[$(voltage_branches_to_system_vector_index[branch])])
                                push!(subexprs, expr)
                            end
                        end
                        push!(coupling_vector_exprs, :([$(subexprs...)]))
                    end
                    coupling_vector_exprs
                end))...)
            ]
        end
    end

    @eval begin
        $couplings_qualifier(x::Vector{Float64}) = begin
            v, i = $unpack_system_vector(x)
            return $couplings(v, i)
        end
    end

    residual_qualifier = gensym("residual")
    residual = @eval begin
        $residual_qualifier(components::Tuple{$(component_types...)}, v::Vector{Float64}, i::Vector{Float64}, τ₁::Float64, τ₂::Float64)::Vector{Float64} = begin
            coupling_vectors = $(couplings)(v, i)

            outputs = [output(component, coupling_vector, τ₁, τ₂)
                       for (component, coupling_vector) in zip(components, coupling_vectors)]

            residual_currents = [
                $(:($(begin
                    residual_current_exprs = []
                    for node in significant_nodes  # Iteration order is important to get a consistent order in the output residual vector!
                        subexprs = []

                        # We are using passive sign convention, which means that branches pointing into a node
                        # have technically a negative - i.e. outgoing - current (which is what is being agreed on here).
                        # However, as long as ingoing and outgoing currents have different signs, it does not really matter
                        # which current direction is negative or not. The Kirchhoff current equations would still hold.
                        for branch in outedges(network, node)
                            if isa(branch, CurrentBranch)
                                component = branches_to_component[branch]
                                i = components_to_index[component]
                                ci = branch_to_coupling_index[branch]
                                push!(subexprs, :(outputs[$i][$ci]))
                            else  # isa(branch, VoltageBranch)
                                index = voltage_branches_to_system_vector_index[branch]
                                push!(subexprs, :(i[$index]))
                            end
                        end

                        for branch in inedges(network, node)
                            if isa(branch, CurrentBranch)
                                component = branches_to_component[branch]
                                i = components_to_index[component]
                                ci = branch_to_coupling_index[branch]
                                push!(subexprs, :(-outputs[$i][$ci]))
                            else  # isa(branch, VoltageBranch)
                                index = voltage_branches_to_system_vector_index[branch]
                                push!(subexprs, :(-i[$index]))
                            end
                        end

                        push!(residual_current_exprs, :(+($(subexprs...))))
                    end
                    residual_current_exprs
                end))...)
            ]

            residual_voltages = [
                $(:($(begin
                    residual_voltage_exprs = []
                    for branch in voltage_branches
                        i = components_to_index[branches_to_component[branch]]
                        subexprs = [:(outputs[$i][$(branch_to_coupling_index[branch])])]
                        if haskey(significant_nodes_to_system_vector_index, branch.target)
                            push!(subexprs, :(-v[$(significant_nodes_to_system_vector_index[branch.target])]))
                        end
                        if haskey(significant_nodes_to_system_vector_index, branch.source)
                            push!(subexprs, :(v[$(significant_nodes_to_system_vector_index[branch.source])]))
                        end

                        push!(residual_voltage_exprs, :(+($(subexprs...))))
                    end
                    residual_voltage_exprs
                end))...)
            ]

            return vcat(residual_currents, residual_voltages)
        end
    end

    @eval begin
        $residual_qualifier(components::Tuple{$(component_types...)}, x::Vector{Float64}, τ₁::Float64, τ₂::Float64)::Vector{Float64} = begin
            v, i = $unpack_system_vector(x)
            return $residual(components, v, i, τ₁, τ₂)
        end
    end

    CircuitModel(
        length(significant_nodes) + length(voltage_branches),
        tuple(components...),
        Dict(pin => significant_nodes_to_system_vector_index[node]
             for (pin, node) in pinmap
             if haskey(significant_nodes_to_system_vector_index, node)),
        Dict((branches_to_component[branch], branch_to_coupling_index[branch]) => length(significant_nodes) + i
             for (i, branch) in enumerate(voltage_branches)),
        partitioning,
        CompiledCircuitModelFunctions(
            unpack_system_vector,
            couplings,
            residual,
        ),
    )
end

function get_events(model::CircuitModel, t₁::Float64, t₂::Float64)::Vector{Float64}
    return [event for event in (next_event(component, t₁, t₂) for component in model.components)
            if !isnothing(event) && t₁ ≤ event ≤ t₂]
end

function get_next_event(model::CircuitModel, t₁::Float64, t₂::Float64)::Union{Float64, Nothing}
    events = get_events(model, t₁, t₂)
    return isempty(events) ? nothing : min(events...)
end

# This macro is highly dependent on the code below for simulate! and reuses internal variables.
macro simulate_and_store(τ₁, τ₂)
    esc(quote
        # Solve.
        result = nlsolve(x₁) do F, x
            F[:] = model.functions.residual(model.components, x, $τ₁, $τ₂)
        end

        # Store results.
        push!(solutions, result.zero)
        push!(times, $τ₂)

        # Update components.
        for (component, coupling_vector) in zip(model.components, model.functions.couplings(result.zero))
            update!(component, coupling_vector, $τ₁, $τ₂)
            push!(states[component], get_state(component))
        end

        # Use current solution as initial guess for next iteration.
        x₁ = result.zero
    end)
end

export simulate!
function simulate!(model::CircuitModel, t₁::Float64, t₂::Float64, steps::Int)::SimulationResult
    # Result lists later stuffed into the result object.
    times = Vector{Float64}()
    solutions = Vector{Vector{Float64}}()
    states = Dict{Component, Vector{Vector{Float64}}}(component => Vector{Vector{Float64}}() for component in model.components)

    # Simulation time points.
    ts = Vector{Float64}()
    if steps > 1
        append!(ts, range(t₁, t₂, length=steps))
    end

    # Event handling is special in the region [t₁, t₁+ERP].
    # Events before t0 must be considered too, since they resolve inside the desired calculation range.
    # All events' (that have an influence, thus (t₁-ERP, t₁+ERP)) post-resolution points must be staged for
    # analysis.
    prepend!(ts, get_events(model, nextfloat(t₁ - ERP), prevfloat(t₁ + ERP)) .+ ERP)  # This is not a typo, it is both t₁.
    sort!(ts)

    x₁ = zeros(model.systemsize)

    # Initial time simulation. Needs to be performed explicitly before the
    # main simulation loop because of the redundant simulation check where τ₁ == τ₂.
    @simulate_and_store(t₁, t₁)

    if !isempty(ts)
        # At this point, if ts is not empty, it contains at least 2 time points to simulate,
        # so the following iterate calls won't throw.
        τ₁, iterstate = iterate(ts)
        τ₂, iterstate = iterate(ts, iterstate)

        while τ₂ ≤ t₂
            if τ₁ == τ₂
                # Sometimes event times and externally desired time steps fall exactly on the same spot. In this case,
                # skip the redundant simulation.
                let nextval = iterate(ts, iterstate)
                    if isnothing(nextval)
                        break
                    end
                    τ₂, iterstate = nextval
                end
                continue
            end

            τe₁ = nextfloat(τ₁ + ERP)
            τe₂ = prevfloat(τ₂ + ERP)
            event = get_next_event(model, τe₁, τe₂)

            if isnothing(event)
                @simulate_and_store(τ₁, τ₂)

                τ₁ = τ₂

                let nextval = iterate(ts, iterstate)
                    if isnothing(nextval)
                        break
                    end
                    τ₂, iterstate = nextval
                end
            else
                τe₁ = event - ERP
                τe₂ = event + ERP

                @simulate_and_store(τ₁, τe₁)

                if τe₂ < τ₂
                    τ₂ = τe₂
                    insert!(ts, iterstate-1, τe₂)
                else
                    insort!(ts, τe₂, iterstate-1, lastindex(ts))
                end

                τ₁ = τe₁
            end
        end
    end

    return SimulationResult(
        times,
        transpose(hcat(solutions...)),
        model.pins_to_system_index,
        model.voltage_branch_coupling_index_to_system_index,
        Dict(component => transpose(hcat(s...)) for (component, s) in states),
    )
end

function statevector(model::CircuitModel)::Vector{Float64}
    statevector = Vector{Float64}()
    for component in model.components
        append!(statevector, get_state(component))
    end
    return statevector
end

function statevector(model::CircuitModel, state::Vector{Float64})
    i = 1
    for (component, statesize) in zip(model.components, model.state_partitioning)
        set_state!(component, state[i:i+statesize-1])
        i += statesize
    end
end

export steadystate!
function steadystate!(model::CircuitModel, t₀::Float64, T::Float64, steps::Int)::SteadyStateSimulationResult
    result = nlsolve(statevector(model)) do F, s₁
        statevector(model, s₁)
        simulate!(model, t₀, t₀ + T, steps)
        s₂ = statevector(model)

        F[:] = s₂ - s₁
    end

    steadystate = result.zero

    statevector(model, steadystate)
    steadystatedict = Dict{Component, Vector{Float64}}(component => get_state(component) for component in model.components)

    sim = simulate!(model, t₀, t₀ + T, steps)

    return SteadyStateSimulationResult(
        sim,
        steadystatedict,
    )
end

export simulate_efm!
simulate_efm!(model::CircuitModel, t₀::Float64, T::Float64, skips::Int, intervals::Int, steps::Int)::EFMSimulationResult = simulate_efm!(model, t₀, T, repeated(skips, intervals), steps)
function simulate_efm!(model::CircuitModel, t₀::Float64, T::Float64, skips, steps::Int)::EFMSimulationResult
    results = Vector{SimulationResult}()

    t₁l = t₀
    t₂l = t₁l + T

    s₁l = statevector(model)
    sim = simulate!(model, t₁l, t₂l, steps)
    s₂l = statevector(model)

    push!(results, sim)

    for skip::Integer in skips
        t₁r = t₂l + skip * T
        t₂r = t₁r + T
        Δsl = s₂l - s₁l

        result = nlsolve(s₂l + skip * Δsl) do F, s₁r  # Initial guess: Forward-Euler
            statevector(model, s₁r)
            simulate!(model, t₁r, t₂r, steps)
            s₂r = statevector(model)

            F[:] = s₂l - s₁r + 0.5 * skip * (Δsl + s₂r - s₁r)
        end

        statevector(model, result.zero)
        sim = simulate!(model, t₁r, t₂r, steps)

        push!(results, sim)

        s₁l = result.zero
        s₂l = statevector(model)
        t₁l = t₁r
        t₂l = t₂r
    end

    return EFMSimulationResult(
        results,
    )
end

efm_characteristic_frequencies(Ts) = (
    Float64(lcm((rational for rational in (rationalize(T, tol=eps(T) * 65536.0) for T in Ts)
                 if numerator(rational) != 0)...)),
    min(Ts...),
)

export multirate_steadystate!
function multirate_steadystate!(model::CircuitModel, t₀::Float64, Ts, steps::Int, intervals::Int)::MultirateSteadyStateSimulationResult
    if intervals < 2
        throw(ArgumentError("intervals must be 2 or greater"))
    end
    Tslow, Tfast = efm_characteristic_frequencies(Ts)
    return multirate_steadystate!(model, t₀, Tslow, Tfast, steps, range(0.0, 1.0, length=intervals)[begin+1:end-1])
end
function multirate_steadystate!(model::CircuitModel, t₀::Float64, Ts, steps::Int, intervaldistribution)::MultirateSteadyStateSimulationResult
    Tslow, Tfast = efm_characteristic_frequencies(Ts)
    return multirate_steadystate!(model, t₀, Tslow, Tfast, steps, intervaldistribution)
end
function multirate_steadystate!(model::CircuitModel, t₀::Float64, Tslow::Float64, Tfast::Float64, steps::Int, intervaldistribution)::MultirateSteadyStateSimulationResult
    intervaldistribution = filter(intervaldistribution) do x
        0.0 < x < 1.0
    end
    sort!(intervaldistribution)
    prepend!(intervaldistribution, 0.0)
    push!(intervaldistribution, 1.0)
    skips = [s for s in diff(floor.(Int, intervaldistribution .* (Tslow ÷ Tfast - 1))) if s > 0] .- 1

    result = nlsolve(statevector(model)) do F, s₁
        statevector(model, s₁)
        simulate_efm!(model, t₀, Tfast, skips, steps)
        s₂ = statevector(model)

        F[:] = s₂ - s₁
    end

    steadystate = result.zero

    statevector(model, steadystate)
    steadystatedict = Dict{Component, Vector{Float64}}(component => get_state(component) for component in model.components)

    sim = simulate_efm!(model, t₀, Tfast, skips, steps)

    return MultirateSteadyStateSimulationResult(
        sim,
        steadystatedict,
    )
end
