import Base: length, show, summarysize
import Dierckx: Spline1D
using Interpolations


export SimulationResult
struct SimulationResult
    times::Vector{Float64}
    solutions::Array{Float64, 2}
    pins_to_system_index::Dict{Pin, Int}
    voltage_branch_coupling_index_to_system_index::Dict{Tuple{Component, Int}, Int}
    states::Dict{Component, Array{Float64, 2}}
end

export get_times
get_times(sim::SimulationResult)::Vector{Float64} = sim.times
export get_voltages
get_voltages(sim::SimulationResult, pin::Pin)::Vector{Float64} = haskey(sim.pins_to_system_index, pin) ? sim.solutions[:,sim.pins_to_system_index[pin]] : zeros(length(sim.times))
get_voltages(sim::SimulationResult, source::Pin, target::Pin)::Vector{Float64} = get_voltages(sim, target) .- get_voltages(sim, source)
export get_currents
get_currents(sim::SimulationResult, component::Component, coupling_index::Int)::Vector{Float64} = sim.solutions[:,sim.voltage_branch_coupling_index_to_system_index[(component, coupling_index)]]
export get_states
get_states(sim::SimulationResult, component::Component)::Array{Float64, 2} = sim.states[component]
get_states(sim::SimulationResult, component::Component, index::Int)::Vector{Float64} = get_states(sim, component)[:,index]

length(result::SimulationResult) = length(result.times)
function show(io::IO, result::SimulationResult)
    datapoints = length(result)
    print(io, "$(typeof(result))($datapoints data point$(datapoints == 1 ? "" : "s"), $(summarysize(result)) bytes)")
end

export SteadyStateSimulationResult
struct SteadyStateSimulationResult
    result::SimulationResult
    steadystate::Dict{Component, Vector{Float64}}
end

export get_times
get_times(sim::SteadyStateSimulationResult)::Vector{Float64} = get_times(sim.result)
export get_voltages
get_voltages(sim::SteadyStateSimulationResult, pin::Pin)::Vector{Float64} = get_voltages(sim.result, pin)
get_voltages(sim::SteadyStateSimulationResult, source::Pin, target::Pin)::Vector{Float64} = get_voltages(sim.result, source, target)
get_voltages(sim::SteadyStateSimulationResult, component::Component)::Vector{Float64} = get_voltages(sim.result, component)
export get_currents
get_currents(sim::SteadyStateSimulationResult, component::Component, coupling_index::Int)::Vector{Float64} = get_currents(sim.result, component, coupling_index)
get_currents(sim::SteadyStateSimulationResult, component::Component)::Vector{Float64} = get_currents(sim.result, component)
export get_states
get_states(sim::SteadyStateSimulationResult, component::Component)::Array{Float64, 2} = get_states(sim.result, component)
get_states(sim::SteadyStateSimulationResult, component::Component, index::Int)::Vector{Float64} = get_states(sim.result, component, index)
export get_steadystate
get_steadystate(sim::SteadyStateSimulationResult, component::Component)::Vector{Float64} = sim.steadystate[component]

length(result::SteadyStateSimulationResult) = length(result.result)
function show(io::IO, result::SteadyStateSimulationResult)
    datapoints = length(result)
    print(io, "$(typeof(result))($datapoints data point$(datapoints == 1 ? "" : "s"), $(summarysize(result)) bytes)")
end

export EFMSimulationResult
struct EFMSimulationResult
    results::Vector{SimulationResult}
end

function interpolate_efm_intervals(xss, yss)::Array{Float64, 2}
    interpx = Vector{Float64}(undef, 2 * length(xss))
    interpy = Vector{Float64}(undef, length(interpx))
    i = 1
    for (xs, ys) in zip(xss, yss)
        interpx[i] = xs[begin]
        interpy[i] = ys[begin]
        i += 1
        interpx[i] = xs[end]
        interpy[i] = ys[end]
        i += 1
    end
    interp = Spline1D(interpx, interpy)

    interpolated_xs = Vector{Float64}()
    append!(interpolated_xs, xss[begin])
    interpolated_ys = Vector{Float64}()
    append!(interpolated_ys, yss[begin])

    xs₁ = xss[begin]
    ys₁ = yss[begin]
    corrected_left_interval_interp = extrapolate(interpolate((xs₁,), (@. ys₁ - interp(xs₁)), Gridded(Linear())), Periodic())
    for (xs₂, ys₂) in zip((@view xss[begin+1:end]), (@view yss[begin+1:end]))
        spread_left_interval_xs = Vector{Float64}()
        i = 1
        while true
            offset = i * (xs₁[end] - xs₁[begin])
            for x in @view xs₁[begin+1:end]
                xn = offset + x
                if xn ≥ xs₂[begin]
                    @goto nestedloop_breakout_1
                end
                push!(spread_left_interval_xs, xn)
            end
            i += 1
        end
        @label nestedloop_breakout_1

        spread_right_interval_xs = Vector{Float64}()
        i = 1
        while true
            offset = i * (xs₂[begin] - xs₂[end])
            for x in reverse(@view xs₂[begin:end-1])
                xn = offset + x
                if xn ≤ xs₁[end]
                    @goto nestedloop_breakout_2
                end
                push!(spread_right_interval_xs, xn)
            end
            i += 1
        end
        @label nestedloop_breakout_2

        compacted_xs = reduce(sort(vcat(spread_left_interval_xs, spread_right_interval_xs)), init=Vector{Float64}()) do l, r
            if isempty(l) || l[end] ≉ r
                push!(l, r)
            end
            l
        end

        corrected_right_interval_interp = extrapolate(interpolate((xs₂,), (@. ys₂ - interp(xs₂)), Gridded(Linear())), Periodic())
        linear_ramp = interpolate(([xs₁[end], xs₂[begin]],), [0.0, 1.0], Gridded(Linear()))

        append!(interpolated_xs, compacted_xs)
        append!(interpolated_ys, @. interp(compacted_xs) + corrected_left_interval_interp(compacted_xs) * linear_ramp(compacted_xs) + corrected_right_interval_interp(compacted_xs) * (1.0 - linear_ramp(compacted_xs)))

        append!(interpolated_xs, xs₂)
        append!(interpolated_ys, ys₂)

        xs₁ = xs₂
        ys₁ = ys₂
        corrected_left_interval_interp = corrected_right_interval_interp
    end

    return hcat(interpolated_xs, interpolated_ys)
end

export get_interval_times
get_interval_times(sim::EFMSimulationResult)::Vector{Vector{Float64}} = [get_times(r) for r in sim.results]
export get_times
get_times(sim::EFMSimulationResult)::Vector{Float64} = flatten(get_interval_times(sim))
get_times(sim::EFMSimulationResult, interval::Int)::Vector{Float64} = get_times(sim.results[interval])
export get_interval_voltages
get_interval_voltages(sim::EFMSimulationResult, pin::Pin)::Vector{Vector{Float64}} = [get_voltages(r, pin) for r in sim.results]
get_interval_voltages(sim::EFMSimulationResult, source::Pin, target::Pin)::Vector{Vector{Float64}} = [get_voltages(r, source, target) for r in sim.results]
get_interval_voltages(sim::EFMSimulationResult, component::Component)::Vector{Vector{Float64}} = [get_voltages(r, component) for r in sim.results]
export get_voltages
get_voltages(sim::EFMSimulationResult, pin::Pin)::Vector{Float64} = flatten(get_interval_voltages(sim, pin))
get_voltages(sim::EFMSimulationResult, source::Pin, target::Pin)::Vector{Float64} = flatten(get_interval_voltages(sim, source, target))
get_voltages(sim::EFMSimulationResult, component::Component)::Vector{Float64} = flatten(get_interval_voltages(sim, component))
export get_interpolated_voltages
get_interpolated_voltages(sim::EFMSimulationResult, pin::Pin)::Array{Float64, 2} = interpolate_efm_intervals(get_interval_times(sim), get_interval_voltages(sim, pin))
get_interpolated_voltages(sim::EFMSimulationResult, source::Pin, target::Pin)::Array{Float64, 2} = interpolate_efm_intervals(get_interval_times(sim), get_interval_voltages(sim, source, target))
get_interpolated_voltages(sim::EFMSimulationResult, component::Component)::Array{Float64, 2} = interpolate_efm_intervals(get_interval_times(sim), get_interval_voltages(sim, component))
export get_interval_currents
get_interval_currents(sim::EFMSimulationResult, component::Component, coupling_index::Int)::Vector{Vector{Float64}} = [get_currents(r, component, coupling_index) for r in sim.results]
get_interval_currents(sim::EFMSimulationResult, component::Component)::Vector{Vector{Float64}} = [get_currents(r, component) for r in sim.results]
export get_currents
get_currents(sim::EFMSimulationResult, component::Component, coupling_index::Int)::Vector{Float64} = flatten(get_interval_currents(sim, component, coupling_index))
get_currents(sim::EFMSimulationResult, component::Component)::Vector{Float64} = flatten(get_interval_currents(sim, component))
export get_interpolated_currents
get_interpolated_currents(sim::EFMSimulationResult, component::Component, coupling_index::Int)::Array{Float64, 2} = interpolate_efm_intervals(get_interval_times(sim), get_interval_currents(sim, component, coupling_index))
get_interpolated_currents(sim::EFMSimulationResult, component::Component)::Array{Float64, 2} = interpolate_efm_intervals(get_interval_times(sim), get_interval_currents(sim, component))
export get_interval_states
get_interval_states(sim::EFMSimulationResult, component::Component)::Vector{Array{Float64, 2}} = [get_states(r, component) for r in sim.results]
get_interval_states(sim::EFMSimulationResult, component::Component, index::Int)::Vector{Vector{Float64}} = [get_states(r, component, index) for r in sim.results]
export get_states
get_states(sim::EFMSimulationResult, component::Component)::Array{Float64, 2} = flatten(get_interval_states(sim, component))
get_states(sim::EFMSimulationResult, component::Component, index::Int)::Vector{Float64} = flatten(get_interval_states(sim, component, index))
export get_interpolated_states
get_interpolated_states(sim::EFMSimulationResult, component::Component)::Array{Float64, 2} = interpolate_efm_intervals(get_interval_times(sim), get_interval_states(component))
get_interpolated_states(sim::EFMSimulationResult, component::Component, index::Int)::Array{Float64, 2} = interpolate_efm_intervals(get_interval_times(sim), get_interval_states(component, index))

length(result::EFMSimulationResult) = sum(map(length, result.results))
function show(io::IO, result::EFMSimulationResult)
    datapoints = length(result)
    print(io, "$(typeof(result))($datapoints data point$(datapoints == 1 ? "" : "s"), $(summarysize(result) + sum(map(summarysize, result.results))) bytes)")
end

export MultirateSteadyStateSimulationResult
struct MultirateSteadyStateSimulationResult
    result::EFMSimulationResult
    steadystate::Dict{Component, Vector{Float64}}
end

export get_interval_times
get_interval_times(sim::MultirateSteadyStateSimulationResult)::Vector{Vector{Float64}} = get_interval_times(sim.result)
export get_times
get_times(sim::MultirateSteadyStateSimulationResult)::Vector{Float64} = get_times(sim.result)
get_times(sim::MultirateSteadyStateSimulationResult, interval::Int) = get_times(sim.results, interval)
export get_interval_voltages
get_interval_voltages(sim::MultirateSteadyStateSimulationResult, pin::Pin)::Vector{Vector{Float64}} = get_interval_voltages(sim.result, pin)
get_interval_voltages(sim::MultirateSteadyStateSimulationResult, source::Pin, target::Pin)::Vector{Vector{Float64}} = get_interval_voltages(sim.result, source, target)
get_interval_voltages(sim::MultirateSteadyStateSimulationResult, component::Component)::Vector{Vector{Float64}} = get_interval_voltages(sim.result, component)
export get_voltages
get_voltages(sim::MultirateSteadyStateSimulationResult, pin::Pin)::Vector{Float64} = get_voltages(sim.result, pin)
get_voltages(sim::MultirateSteadyStateSimulationResult, source::Pin, target::Pin)::Vector{Float64} = get_voltages(sim.result, source, target)
get_voltages(sim::MultirateSteadyStateSimulationResult, component::Component)::Vector{Float64} = get_voltages(sim.result, component)
export get_interpolated_voltages
get_interpolated_voltages(sim::MultirateSteadyStateSimulationResult, pin::Pin)::Array{Float64, 2} = get_interpolated_voltages(sim.result, pin)
get_interpolated_voltages(sim::MultirateSteadyStateSimulationResult, source::Pin, target::Pin)::Array{Float64, 2} = get_interpolated_voltages(sim.result, source, target)
get_interpolated_voltages(sim::MultirateSteadyStateSimulationResult, component::Component)::Array{Float64, 2} = get_interpolated_voltages(sim.result, component)
export get_interval_currents
get_interval_currents(sim::MultirateSteadyStateSimulationResult, component::Component, coupling_index::Int)::Vector{Vector{Float64}} = get_interval_currents(sim.result, component, coupling_index)
get_interval_currents(sim::MultirateSteadyStateSimulationResult, component::Component)::Vector{Vector{Float64}} = get_interval_currents(sim.result, component)
export get_currents
get_currents(sim::MultirateSteadyStateSimulationResult, component::Component, coupling_index::Int)::Vector{Float64} = get_currents(sim.result, component, coupling_index)
get_currents(sim::MultirateSteadyStateSimulationResult, component::Component)::Vector{Float64} = get_currents(sim.result, component)
export get_interpolated_currents
get_interpolated_currents(sim::MultirateSteadyStateSimulationResult, component::Component, coupling_index::Int)::Array{Float64, 2} = get_interpolated_currents(sim.result, component, coupling_index)
get_interpolated_currents(sim::MultirateSteadyStateSimulationResult, component::Component)::Array{Float64, 2} = get_interpolated_currents(sim.result, component)
export get_interval_states
get_interval_states(sim::MultirateSteadyStateSimulationResult, component::Component)::Vector{Array{Float64, 2}} = get_interval_states(sim.result, component)
get_interval_states(sim::MultirateSteadyStateSimulationResult, component::Component, index::Int)::Vector{Vector{Float64}} = get_interval_states(sim.result, component, index)
export get_states
get_states(sim::MultirateSteadyStateSimulationResult, component::Component)::Array{Float64, 2} = get_states(sim.result, component)
get_states(sim::MultirateSteadyStateSimulationResult, component::Component, index::Int)::Vector{Float64} = get_states(sim.result, component, index)
export get_interpolated_states
get_interpolated_states(sim::MultirateSteadyStateSimulationResult, component::Component)::Array{Float64, 2} = get_interpolated_states(sim.result, component)
get_interpolated_states(sim::MultirateSteadyStateSimulationResult, component::Component, index::Int)::Array{Float64, 2} = get_interpolated_states(sim.result, component, index)
export get_steadystate
get_steadystate(sim::MultirateSteadyStateSimulationResult, component::Component)::Vector{Float64} = get_steadystate(sim.result, component)

length(result::MultirateSteadyStateSimulationResult) = length(result.result)
function show(io::IO, result::MultirateSteadyStateSimulationResult)
    datapoints = length(result)
    print(io, "$(typeof(result))($datapoints data point$(datapoints == 1 ? "" : "s"), $(summarysize(result)) bytes)")
end
