export Circuit
struct Circuit
    connections::BidirectionalOneToManyMap{Node, Pin}

    Circuit() = new(BidirectionalOneToManyMap{Node, Pin}())
end

export connect!
function connect!(circuit::Circuit, pins::Pin...)
    # Create a new node to connect all pins to.
    node = Node()

    overridepins::Vector{Pin} = []

    for pin in pins
        if haskey(circuit.connections.reverse, pin)
            # In case we connect to pins that already are connected to something else,
            # it behaves like short-circuiting with all the other pins connected to
            # this one pin. So their node information becomes updated too.
            node = circuit.connections.reverse[pin]
            append!(overridepins, circuit.connections.obverse[node])
        else
            push!(overridepins, pin)
        end
    end

    # Override connectivity information on all selected pins.
    for pin in overridepins
        circuit.connections.reverse[pin] = node
    end
end

export disconnect!
function disconnect!(circuit::Circuit, pins::Pin...)
    for pin in pins
        delete!(circuit.connections.reverse, pin)
    end
end
