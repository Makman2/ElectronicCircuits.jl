# Edges are unique.
export MultiDiGraph
struct MultiDiGraph{N, E}
    edges::BidirectionalOneToManyMap{Pair{N, N}, E}
    orientation::BidirectionalMultiMap{N, N}
end

function MultiDiGraph{N, E}() where {N, E}
    return MultiDiGraph(
        BidirectionalOneToManyMap{Pair{N, N}, E}(),
        BidirectionalMultiMap{N, N}(),
    )
end

MultiDiGraph() = MultiDiGraph{Any, Any}()

export add_edge!
function add_edge!(graph::MultiDiGraph{N, E}, source::N, target::N, key::E) where {N, E}
    add_edge!(graph, source => target, key)
end
function add_edge!(graph::MultiDiGraph{N, E}, edge::Pair{N, N}, key::E) where {N, E}
    graph.edges[edge] = key
    graph.orientation[edge.first] = edge.second
end

export remove_edge!
function remove_edge!(graph::MultiDiGraph{N, E}, edge::Pair{N, N}) where {N, E}
    delete!(graph.edges, edge)
    delete!(graph.orientation, edge.first)
end
function remove_edge!(graph::MultiDiGraph{N, E}, key::E) where {N, E}
    remove_edge!(graph, graph.edges.reverse[key])
end

export outneighbors
function outneighbors(graph::MultiDiGraph{N, E}, node::N) where {N, E}
    return haskey(graph.orientation.obverse, node) ? copy(graph.orientation.obverse[node]) : Set{N}()
end

export inneighbors
function inneighbors(graph::MultiDiGraph{N, E}, node::N) where {N, E}
    return haskey(graph.orientation.reverse, node) ? copy(graph.orientation.reverse[node]) : Set{N}()
end

export neighbors
function neighbors(graph::MultiDiGraph{N, E}, node::N) where {N, E}
    s = Set{N}()
    if haskey(graph.orientation.obverse, node)
        union!(s, graph.orientation.obverse[node])
    end
    if haskey(graph.orientation.reverse, node)
        union!(s, graph.orientation.reverse[node])
    end
    return s
end

export outedges
function outedges(graph::MultiDiGraph{N, E}, node::N) where {N, E}
    if haskey(graph.orientation.obverse, node)
        return Set(key
                   for target in graph.orientation.obverse[node]
                   for key in graph.edges[node => target])
    else
        return Set{E}()
    end
end

export inedges
function inedges(graph::MultiDiGraph{N, E}, node::N) where {N, E}
    if haskey(graph.orientation.reverse, node)
        return Set(key
                   for source in graph.orientation.reverse[node]
                   for key in graph.edges[source => node])
    else
        return Set{E}()
    end
end

export edges
function edges(graph::MultiDiGraph{N, E}, node::N) where {N, E}
    return union(inedges(graph, node), outedges(graph, node))
end

export nodes
function nodes(graph::MultiDiGraph)
    return union(keys(graph.orientation.obverse), keys(graph.orientation.reverse))
end

export weakly_connected_components
function weakly_connected_components(graph::MultiDiGraph{N, E}) where {N, E}
    allnodes = nodes(graph)
    partitions = Vector{Set{N}}()
    while !isempty(allnodes)
        partition = Set{N}()
        checknodes = Set()
        push!(checknodes, pop!(allnodes))
        while !isempty(checknodes)
            checknode = pop!(checknodes)
            if checknode ∉ partition
                push!(partition, checknode)

                for newnode in outneighbors(graph, checknode)
                    delete!(allnodes, newnode)
                    push!(checknodes, newnode)
                end
                for newnode in inneighbors(graph, checknode)
                    delete!(allnodes, newnode)
                    push!(checknodes, newnode)
                end
            end
        end
        push!(partitions, partition)
    end
    return partitions
end
